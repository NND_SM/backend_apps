package com.gruposm.backend.rest;

import com.gruposm.backend.domain.AppCode;
import com.gruposm.backend.exception.*;
import com.gruposm.backend.exception.BadRequestException;
import com.gruposm.backend.exception.NotFoundException;
import com.gruposm.backend.persistence.ConnectionFactory;
import com.gruposm.backend.service.AppCodeService;
import com.gruposm.backend.utils.AuthUtils;
import com.gruposm.backend.utils.CodeUtils;
import com.gruposm.backend.utils.Config;
import org.apache.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.Arrays;
import java.util.List;



/**
 * REST Data Layer
 * <p>
 * ...
 * 
 * 
 * @version 0.1
 * @date Jun 23, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
@Path("/")
public class DataController {

	/** Log4Java */
	private static final Logger log = Logger.getLogger(DataController.class);
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService() {
		String result = "RESTService Successfully started..";
 
		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}
	
	@GET
	@Path("/clearCache/{token}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response clearCache(@PathParam("token") String token) {
		if (Config.getAdminToken().equals(token)) {
			ConnectionFactory.clearCache();
			String result = "MyBatis cache cleared successfully!";
			return Response.status(HttpStatus.OK_200).entity(result).build();			
		} else {
			String error = "A security token is needed to perform administrative tasks";
			return Response.status(HttpStatus.UNAUTHORIZED_401).entity(error).build();
		}
	}


	/**
	 * Activate Code
	 */
	@POST
	@Path("/activate")
	@Produces(MediaType.APPLICATION_JSON)
	public Response doActivate(@QueryParam("uuid_code") String uuid_code,
								@QueryParam("user") String userId,
								@QueryParam("token") String token) {
		try {
            // Check security synchronously...
            if (Config.isTokenSecurityEnabled()) {
                // Check token parameter
                if (token == null || token.trim().equals("")) {
                    throw new BadRequestException("Not exist or empty (token)");
                }
                AuthUtils.getInstance().validateToken(uuid_code, token);
            }

			// Check user parameter
			if (userId == null || userId.trim().equals("")) {
				throw new BadRequestException("Not exist or empty (user)");
			}

            // Check uuid_code parameter
            if (uuid_code == null || uuid_code.trim().equals("")) {
                throw new BadRequestException("Not exist or empty (uuid_code)");
            }

			// Get appCode
			AppCodeService as = new AppCodeService();
			AppCode appCode = as.getBy(uuid_code, userId);
			if (appCode == null) {
				throw new NotFoundException("Codigo de aplicación no encontrado");
			}

			// Activate appCode
			as.activate(appCode, userId);

			// Update database appCode.
			as.update(appCode);

			// Response OK
			String jsonOK = "{\"result\":\"OK\"}";
			return Response.status(HttpStatus.OK_200).entity(jsonOK).build();

		} catch (BadRequestException e) {   // ResponseInvalid parameter.
			log.error("<activate>: Error activate: " + e.getMessage());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.BAD_REQUEST_400).entity(jsonError).build();
		} catch (AuthException e) {         // Response invalid token.
			log.error("<activate>: Error activate: " + e.getMessage());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.UNAUTHORIZED_401).entity(jsonError).build();
		} catch (NotFoundException e) {     // Response invalid token.
			log.error("<activate>: Error activate: " + e.toString());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.NOT_FOUND_404).entity(jsonError).build();
		} catch (LimitException e) {        // Response Exede the limit of activations.
			log.error("<activate>: Error activate: " + e.toString());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.NOT_ACCEPTABLE_406).entity(jsonError).build();
		} catch (ExpirationException e) { // AppCode expired OR not Started
			log.error("<activate>: Error check: " + e.toString());
			String jsonError = "{\"error\":" + e.getMessage() + "}";
			return Response.status(HttpStatus.FORBIDDEN_403).entity(jsonError).build();
		} catch (Exception e) {             // Response internal error.
			log.error("<activate>: Error activate: " + e.toString());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.INTERNAL_SERVER_ERROR_500).entity(jsonError).build();
		}
	}


    /**
	 * Get a previously saved uuid.
	 * <p>
	 * Must validate against AuthUtils with a valid sessionId in case Session is Required
	 * at Config level...
	 *
	 * @return JSON with the associated MainEntity
	 */
	@GET
	@Path("/check")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@QueryParam("uuid_code") String uuid_code,
						@QueryParam("user") String userId,
						@QueryParam("token") String token,
						@Context HttpHeaders headers) {
		try {
			// Check token
			if (token == null || token.trim().equals("")) {
				throw new BadRequestException("Not exist or empty (token)");
			}
			// Check userID
			if (userId == null || userId.trim().equals("")) {
				throw new BadRequestException("Not exist or empty (user)");
			}
			// Check security synchronously...
			if (Config.isTokenSecurityEnabled()) {
				if (uuid_code == null || uuid_code.trim().equals("")) {
					throw new BadRequestException("Not exist or empty (uuid_code)");
				}
				AuthUtils.getInstance().validateToken(uuid_code, token);
			}

			// Check appCode
			AppCodeService as = new AppCodeService();
			AppCode appCode = as.getBy(uuid_code, userId);
			if (appCode == null) {
				throw new NotFoundException("Codigo de aplicación no encontrado");
			}

			// Check active appCode
			if (appCode.getActivationCount() == 0) {
				throw new InactiveException("Aplication code is not active");
			}

			// Check Expiration appCode
            as.checkExpiration(appCode);

			// Update appCode into DB.
            as.update(appCode);

			String jsonOK = "{\"result\":\"OK\", \"expiration\":" + appCode.toExpirationString() +  "}";
			return Response.status(HttpStatus.OK_200).entity(jsonOK).build();
		} catch (BadRequestException e) { // Null or Empty request fields
			log.error("<check>: Error check: " + e.getMessage());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.BAD_REQUEST_400).entity(jsonError).build();
		} catch (AuthException e) { // Invalid Token.
			log.error("<check>: Error check: " + e.getMessage());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.UNAUTHORIZED_401).entity(jsonError).build();
		} catch (NotFoundException e) { // AppCode not found
			log.error("<check>: Error check: " + e.toString());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.NOT_FOUND_404).entity(jsonError).build();
		} catch (InactiveException e) { // AppCode not active
			log.error("<check>: Error check: " + e.toString());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.NOT_ACCEPTABLE_406).entity(jsonError).build();
		} catch (ExpirationException e) { // AppCode expired OR not Started
			log.error("<check>: Error check: " + e.toString());
			String jsonError = "{\"error\":" + e.getMessage() + "}";
			return Response.status(HttpStatus.FORBIDDEN_403).entity(jsonError).build();
		} catch (Exception e) { // Server Errors
			log.error("<check>: Error check: " + e.toString());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.INTERNAL_SERVER_ERROR_500).entity(jsonError).build();
		}
	}

	/**
	 * Gets default JSON languages...
	 * <p>
	 * Must validate against AuthUtils with a valid sessionId in case Session is Required
	 * at Config level...
	 *
	 * @return default JSON under assets/json/...
	 */
	@GET
	@Path("/version")
	@Produces(MediaType.APPLICATION_JSON)
	public Response version(@QueryParam("code") String code,
							@QueryParam("token") String token,
							@Context HttpHeaders headers) {
		return versionfile(Config.getDefaultAssetsJsonFile(), code, token, headers);
	}

	/**
	 * Get a JSON config file specified by json parameter...
	 * <p>
	 * Must validate against AuthUtils with a valid sessionId in case Session is Required
	 * at Config level...
	 *
	 * @return JSON ...
	 */
	@GET
	@Path("/versionfile")
	@Produces(MediaType.APPLICATION_JSON)
	public Response versionfile(@QueryParam("json") String json,
							@QueryParam("code") String code,
							@QueryParam("token") String token,
							@Context HttpHeaders headers) {
		try {
			// Check security synchronously...
			if (Config.isTokenSecurityEnabled()) {
				AuthUtils.getInstance().validateToken(code, token);
			}

			AppCodeService as = new AppCodeService();
			String jsonToReturn = as.getLastVersion(json);
			return Response.status(HttpStatus.OK_200).entity(jsonToReturn).build();
		} catch (AuthException e) {
			log.error("<checkExpiration>: Error versionfile: " + e.getMessage());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.UNAUTHORIZED_401).entity(jsonError).build();
		} catch (NotFoundException e) {
			log.error("<checkExpiration>: Error versionfile JSON: " + e.toString());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.NOT_FOUND_404).entity(jsonError).build();
		} catch (Exception e) {
			log.error("<checkExpiration>: Error versionfile JSON: " + e.toString());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.INTERNAL_SERVER_ERROR_500).entity(jsonError).build();
		}
	}

	/**
	 * Generates random codes for each application in the list.
	 *
	 * @return default JSON under assets/json/...
	 */
	@POST
	@Path("/gen")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response gen(@QueryParam("apps") String apps,
						@QueryParam("amountsPerApps") Integer amountPerApps,
						@QueryParam("expirationType") String expirationType,
						@Context Request req) {
		try {
			CodeUtils codeUtils = CodeUtils.getInstance();
			List<String> appsList = Arrays.asList(apps.split(","));
			codeUtils.genAppsCodes(appsList, amountPerApps, expirationType);
			return Response.status(HttpStatus.OK_200).entity("").build();
		} catch (Exception e) {
			log.error("<checkExpiration>: Error codes generation: " + e.toString());
			String jsonError = "{\"error\":\"" + e.getMessage() + "\"}";
			return Response.status(HttpStatus.NOT_FOUND_404).entity(jsonError).build();
		}

	}

	
}
