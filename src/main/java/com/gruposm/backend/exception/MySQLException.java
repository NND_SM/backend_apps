package com.gruposm.backend.exception;

/**
 * MySQL Exception
 * 
 * @version 0.1
 * @date Jun 24, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class MySQLException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1840451845639572517L;
	
	/**
	 *
     */
    public MySQLException()
    {
    	super();
    }
    
	/**
     * Constructor with a detail message.
     *
     * @param s the detail message
     */
    public MySQLException(String s)
    {
    	super(s);
    }
}
