package com.gruposm.backend.exception;

import org.apache.log4j.Logger;

public class InactiveException extends BackendException{
    /** */
    private static final long serialVersionUID = 7294245640719955735L;

    /** Log4Java */
    private static final Logger log = Logger.getLogger(InactiveException.class);

    /**
     *
     */
    public InactiveException()
    {
        super();
    }

    /**
     * Constructor with a detail message.
     *
     * @param s the detail message
     */
    public InactiveException(String s) {
        super(s);
        log.warn("<InactiveException>: " + s);
    }
}
