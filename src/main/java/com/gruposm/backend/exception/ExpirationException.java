package com.gruposm.backend.exception;

public class ExpirationException extends BackendException {
    private static final long serialVersionUID = -1840451845639572000L;


    /**
     *
     */
    public ExpirationException() {
        super();
    }

    /**
     * Constructor with a detail message.
     *
     * @param s the detail message
     */
    public ExpirationException(String s) {
        super(s);
    }

}
