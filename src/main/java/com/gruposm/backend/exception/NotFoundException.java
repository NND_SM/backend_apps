package com.gruposm.backend.exception;

import com.gruposm.backend.utils.AuthUtils;
import org.apache.log4j.Logger;

public class NotFoundException extends BackendException {

	/** */
	private static final long serialVersionUID = 3674248640719955731L;

	/** Log4Java */
	private static final Logger log = Logger.getLogger(AuthUtils.class);

	/**
     *
     */
    public NotFoundException()
    {
    	super();
    }

	/**
     * Constructor with a detail message.
     *
     * @param s the detail message
     */
    public NotFoundException(String s)
    {
    	super(s);
    	log.warn("<NotFoundException>: " + s);
    }

}
