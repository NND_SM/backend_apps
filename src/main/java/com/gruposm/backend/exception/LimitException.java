package com.gruposm.backend.exception;

import org.apache.log4j.Logger;

public class LimitException extends BackendException {

    /** */
    private static final long serialVersionUID = 8894248640719955735L;

    /** Log4Java */
    private static final Logger log = Logger.getLogger(LimitException.class);

    /**
     *
     */
    public LimitException() {
        super();
    }

    /**
     * Constructor with a detail message.
     *
     * @param s the detail message
     */
    public LimitException(String s) {
        super(s);
        log.warn("<LimitException>: " + s);
    }

}