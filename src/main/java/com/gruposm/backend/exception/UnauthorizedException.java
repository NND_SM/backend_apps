package com.gruposm.backend.exception;

import org.apache.log4j.Logger;

import com.gruposm.backend.utils.AuthUtils;

public class UnauthorizedException extends AuthException {

	/** */
	private static final long serialVersionUID = 3674248640719955735L;

	/** Log4Java */
	private static final Logger log = Logger.getLogger(AuthUtils.class);

	/**
     *
     */
    public UnauthorizedException()
    {
    	super();
    }
    
	/**
     * Constructor with a detail message.
     *
     * @param s the detail message
     */
    public UnauthorizedException(String s)
    {
    	super(s);
    	log.warn("<UnauthorizedException>: " + s);
    }

}
