package com.gruposm.backend.exception;



public class BadRequestException extends BackendException {
    private static final long serialVersionUID = -456451845639572000L;


    /**
     *
     */
    public BadRequestException() {
        super();
    }

    /**
     * Constructor with a detail message.
     *
     * @param s the detail message
     */
    public BadRequestException(String s) {
        super(s);
    }

}
