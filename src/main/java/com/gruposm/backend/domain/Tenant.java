package com.gruposm.backend.domain;

import java.io.Serializable;

/**
 * A helper class for Tenancy
 * 
 * @version 0.1
 * @date Nov 14, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class Tenant implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Atributos */
	private String tenant;
	private String description;
	private String fqdn;
	private long createdOn;
	private boolean enabled;

	public Tenant() {
		
	}

	public String getTenant() {
		return tenant;
	}

	public void setTenant(String tenant) {
		this.tenant = tenant;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFqdn() {
		return fqdn;
	}

	public void setFqdn(String fqdn) {
		this.fqdn = fqdn;
	}

	public long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(long createdOn) {
		this.createdOn = createdOn;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "Tenant{" +
				"tenant='" + tenant + '\'' +
				", description='" + description + '\'' +
				", fqdn='" + fqdn + '\'' +
				", createdOn=" + createdOn +
				", enabled=" + enabled +
				'}';
	}
}
