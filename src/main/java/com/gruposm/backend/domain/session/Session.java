package com.gruposm.backend.domain.session;

import java.io.Serializable;

/**
 * A helper class for Sessions
 * 
 * @version 0.1
 * @date Nov 15, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class Session  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** Atributos */
	private String session;
	private String userId;
	private long createdOn;
	private long updatedOn;
	private String attributes;
	private String deviceId;
	private String tenant;
	
	public Session() {
		
	}

	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public long getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(long createdOn) {
		this.createdOn = createdOn;
	}
	public long getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(long updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getAttributes() {
		return attributes;
	}
	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getTenant() {
		return tenant;
	}

	public void setTenant(String tenant) {
		this.tenant = tenant;
	}

	@Override
	public String toString() {
		return "Session{" +
				"session='" + session + '\'' +
				", userId='" + userId + '\'' +
				", createdOn=" + createdOn +
				", updatedOn=" + updatedOn +
				", attributes='" + attributes + '\'' +
				", deviceId='" + deviceId + '\'' +
				", tenant='" + tenant + '\'' +
				'}';
	}
}
