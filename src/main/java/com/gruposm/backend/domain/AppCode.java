package com.gruposm.backend.domain;

import sun.rmi.runtime.Log;

import java.awt.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A helper class for AppCode
 * 
 * @version 0.1
 * @date Nov 14, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class AppCode implements Serializable {

	private static final long serialrsionUID = 1L;

	/** Atributos */
	private String uuidCode;
	private int idApp;
	private Long createdOn;
	private String usedBy;
	private Long usedOn;
	private String expirationType;
	private Long expirationDate;
	private Long expirationStart;
	private int activationCount;
    private int activationLimit;

	public AppCode() {
		
	}

	public String getUuidCode() {
		return uuidCode;
	}

	public void setUuidCode(String uuidCode) {
		this.uuidCode = uuidCode;
	}

	public int getIdApp() {
		return idApp;
	}

	public void setIdApp(int idApp) {
		this.idApp = idApp;
	}

	public Long getCreatedOn() { return createdOn; }

	public void setCreatedOn(Long createdOn) {
		this.createdOn = createdOn;
	}

	public String getUsedBy() {
		return usedBy;
	}

	public void setUsedBy(String usedBy) {
		this.usedBy = usedBy;
	}

	public Long getUsedOn() {
		return usedOn;
	}

	public void setUsedOn(Long usedOn) {
		this.usedOn = usedOn;
	}

	public String getExpirationType() {
		return expirationType;
	}

	public void setExpirationType(String expirationType) {
		this.expirationType = expirationType;
	}

	public Long getExpirationStart() {
		return expirationStart;
	}

	public void setExpirationStart(Long expirationStart) {
		this.expirationStart = expirationStart;
	}

	public Long getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Long expirationDate) {
		this.expirationDate = expirationDate;
	}

    public int getActivationCount() {
        return activationCount;
    }

    public void setActivationCount(int activationCount) {
        this.activationCount = activationCount;
    }

    public int getActivationLimit() {
        return activationLimit;
    }

    public void setActivationLimit(int activationLimit) {
        this.activationLimit = activationLimit;
    }


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		AppCode appCode = (AppCode) o;
		return Objects.equals(uuidCode, appCode.uuidCode);
	}

	@Override
	public int hashCode() {
		return Objects.hash(uuidCode);
	}

	@Override
	public String toString() {
		return "AppCode{" +
				"uuidCode='" + uuidCode + '\'' +
				", idApp=" + idApp +
				", createdOn=" + createdOn +
				", usedBy='" + usedBy + '\'' +
				", usedOn=" + usedOn +
				", expirationType='" + expirationType + '\'' +
				", expirationStart='" + expirationStart + '\'' +
				", expirationDate=" + expirationDate +
				'}';
	}

	public String toExpirationString() {
		return 	expirationType.equals("never")
				? "{" +
						"\"expirationType\":\"" + expirationType + "\"" +
				  "}"
				: "{" +
						"\"expirationType\":\"" + expirationType + "\"" +
						", \"expirationStart\":\"" + expirationStart + "\"" +
						", \"expirationDate\":\"" + expirationDate + "\"" +
				   "}";
	}
}
