package com.gruposm.backend.domain;

import java.io.Serializable;

/**
 * A helper class for Admin
 * 
 * @version 0.1
 * @date Nov 14, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class Admin  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** Atributos */
	private String username;
	private String password;
	private String role;
	private String fname;
	private String lname;
	private boolean enabled;
	private long lastLogin;
	private String tenant;
	
	public Admin() {
		
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public long getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(long lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getTenant() {
		return tenant;
	}

	public void setTenant(String tenant) {
		this.tenant = tenant;
	}

	@Override
	public String toString() {
		return "Admin{" +
				"username='" + username + '\'' +
				", password='" + password + '\'' +
				", role='" + role + '\'' +
				", fname='" + fname + '\'' +
				", lname='" + lname + '\'' +
				", enabled=" + enabled +
				", lastLogin=" + lastLogin +
				", tenant='" + tenant + '\'' +
				'}';
	}
}
