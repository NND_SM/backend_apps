package com.gruposm.backend.utils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.gruposm.backend.domain.Admin;
import com.gruposm.backend.domain.Tenant;
import com.gruposm.backend.domain.session.Session;
import com.gruposm.backend.exception.AuthException;
import com.gruposm.backend.exception.UnauthorizedException;
import com.gruposm.backend.service.AdminService;
import com.gruposm.backend.service.TenantService;
import com.gruposm.backend.utils.thymeleaf.LocaleUtils;
import org.apache.log4j.Logger;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Auth Utils for authorization/authentication
 * 
 * @version 0.1
 * @date Jun 24, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class AuthUtils {

	/** Log4Java */
	private static final Logger log = Logger.getLogger(AuthUtils.class);
	
	/** Singleton */
	private static final AuthUtils instance;
	
	/** Clean sessions each minute... */
	private static final long SLEEP_CLEANER = 60000l;

	/** LocaleUtils */
	private LocaleUtils i18n;
	
	static {
	    instance = new AuthUtils();
	}
	

	/**
	 * 
	 */
	public AuthUtils() {
		i18n = LocaleUtils.getInstance();
	}
	
	public static AuthUtils getInstance() {
		return instance;
	}

	/**
	 * Validate <code>request</code> viability. 
	 * <p>
	 * Can be of two different forms:
	 * <ul>
	 * <li>A request containing username & password, intended for admin purposes</li>
	 * <li>A request containing different parameters & a SHA1 token for checksumming</li>
	 * </ul>
	 * In case <code>request</code> is validated by any means, <code>request</code> will be 
	 * added to a dedicated SessionStore  
	 * 
	 * @param request ...
	 * @param response - In order to submit ...
	 * @throws AuthException in case request cannot be validated
	 */
	public void validate(HttpServletRequest request, HttpServletResponse response) throws AuthException {
		if ((Config.isSessionRequired() && getAttribute(request, Constants.TOKEN) != null) ||
			getAttribute(request, Constants.USER_ID) != null) {
			checkSecurityToken(request);
		} else {
			validateAdmin(request);
		}
	}

	/**
	 * SecurityToken auth wrapper, so we can merge here our custom SSO based token URL and
	 * the one forwarded by App|Mobile devices with a Firebase token
	 *
	 * @param request ...
	 * @throws AuthException ...
	 */
	private void checkSecurityToken(HttpServletRequest request) throws AuthException {
		if (getAttribute(request, Constants.USER_ID) != null) {
			checkStdSecurityToken(request);
		}
	}

	/**
	 * In case no user_id is supplied at QueryString, we assume it is a Firebase token
	 * login, so we will check against Firebase servers if the token supplied is valid.
	 *
	 * @param request ...
	 * @throws AuthException ...
	 */
	private void checkFirebaseSecurityToken(HttpServletRequest request) throws AuthException {
		String idToken = getAttribute(request, Constants.TOKEN);
		String deviceId = getAttribute(request, Constants.DEVICE_ID);
		try {
			boolean checkRevoked = true;
			FirebaseToken fbToken = FirebaseAuth.getInstance().verifyIdTokenAsync(idToken, checkRevoked).get();
			// Token is valid and not revoked.
			// Tengo que darle una sesión válida de usuario dentro de nuestro entorno, siendo:
			// user_id = decodedToken.getEmail()
			// fname = decodedToken.getName()
			// lname = ""
			// ...
			request.setAttribute(Constants.FROM_FIREBASE, "true");
			request.setAttribute(Constants.USER_ID, fbToken.getEmail());
			request.setAttribute(Constants.FNAME, fbToken.getName());
			request.setAttribute(Constants.LNAME, "");
			request.setAttribute(Constants.PICTURE, fbToken.getPicture());
			request.setAttribute(Constants.DEVICE_ID, deviceId);
		} catch (Exception e) {
			log.error("<checkFirebaseSecurityToken>: Error decoding token: " + e.toString());
			throw new AuthException(e.getMessage());
		}
	}

	/**
	 * A standard <code>request</code> should contain information about a uuid instance
	 * or assignment, a user_id, and many other aspects not covered here.
	 * Finally, it _must_ contain a token checksumming all the prior parameters contained
	 * in the proper <code>queryString</code>
	 *
	 * @param request ...
	 * @throws AuthException In case checksumming is incorrect
	 */
	private void checkStdSecurityToken(HttpServletRequest request) throws AuthException {
		String queryString = request.getQueryString();
		String checksum = getAttribute(request, Constants.TOKEN);
		// Limpio checksum del queryString... 
		queryString = queryString.replace("?token=" + checksum, "");
		queryString = queryString.replace("&token=" + checksum, "");
		String intChecksum = "";
		if (checksum != null && !checksum.equals("") && queryString != null && !queryString.equals("")) {
			intChecksum = Utils.getSHA1(queryString + Config.getSecuritySalt());	
		} else {
			if (Config.isSessionRequired()) {
				throw new UnauthorizedException(i18n.getString(request.getLocale(), "auth.void.checksum"));
			} else {
				// Security warning!! No checksum at all checked!
				checksum = intChecksum;
				log.warn("<checkSecurityToken>: Checksum omitted as per config! Please, use with caution!");
			}
		}

		// Control de timeout... (en segundos!)
		if (Config.isSecurityChecksumTimeoutEnabled()) {
			String strTimeout = getAttribute(request, Constants.TIMESTAMP_SHORT);
			if (strTimeout == null || strTimeout.equals("")) {
				log.warn("<checkSecurityToken>: SecurityChecksumTimeout enabled but no "
						+ Constants.TIMESTAMP_SHORT + " parameter passed!!");
				throw new UnauthorizedException(i18n.getString(request.getLocale(), "auth.provide.credentials"));
			}
			long when = Long.parseLong(strTimeout);
			long now = System.currentTimeMillis() / 1000;
			if (now - when > Config.getSecurityChecksumTimeout()) {
				log.warn("<checkSecurityToken>: SecurityChecksumTimeout surpassed: "
						+ (now - when) + " > " + Config.getSecurityChecksumTimeout() + " --> Unauthorized!");
				throw new UnauthorizedException(i18n.getString(request.getLocale(), "auth.provide.credentials"));
			}
		}

		// Right! Valid request -> Get username & store new Session....
		if (checksum.equals(intChecksum)) {
			String userId = getAttribute(request, Constants.USER_ID);
			if (userId == null || userId.equals("")) {
				throw new UnauthorizedException(i18n.getString(request.getLocale(), "auth.void.checksum", Constants.USER_ID));
			} else {
				log.info("<checkSecurityToken>: User " + userId + " validated properly... Logging in...");
			}
		} else {
			throw new UnauthorizedException(i18n.getString(request.getLocale(), "auth.invalid.checksum", checksum, queryString));
		}
	}

	
	/**
	 * Validates a request with username + password ...
	 *
	 * @param request ...
	 * @throws AuthException ...
	 */
	private void validateAdmin(HttpServletRequest request) throws AuthException {
		String username = getAttribute(request, Constants.USERNAME);
		String password = getAttribute(request, Constants.PASSWORD);
		if (username == null || username.equals("") || password == null || password.equals("")) {
			throw new AuthException(i18n.getString(request.getLocale(), "auth.provide.credentials"));
		}
		AdminService as = new AdminService();
		Admin admin = as.getByName(username, getTenantFromRequest(request));
		if (admin == null) {
			throw new AuthException(i18n.getString(request.getLocale(), "auth.admin.notfound"));
		} else {
			if (admin.getPassword().equals(Utils.getSHA1(password))) {
				log.info("<validateAdmin>: " + username + " validated properly... Logging in...");
			} else {
				throw new AuthException(i18n.getString(request.getLocale(), "auth.passwords.notmatch"));
			}
		}
	}


	/**
	 * By Agreement: tenant will be the Fully Qualified Domain Name of the request!
	 *
	 * @see com.gruposm.backend.persistence.mappers.TenantMapper
	 * @param request ...
	 * @return String tenant (FQDN)
	 */
	public String getTenantFromRequest(HttpServletRequest request) throws AuthException {
		TenantService ts = new TenantService();
		Tenant tenant = ts.getByDomain(request.getServerName());
		if (tenant == null) {
			throw new AuthException("Tenant not available!");
		} else {
			return tenant.getTenant();
		}
	}



	/**
	 * Touch Session - Log + update in Session Store...
	 * <p>
	 * Controls wether this session has been more than allowed time under
	 * <code>security.session.timeout.mins</code>, throwing an <code>AuthException</code>
	 * in such case...
	 *
	 * @param session ...
	 * @throws AuthException in case existing session is longer than timeout.mins...
	 */
	private void touchSession(Session session) throws AuthException {
		long lastUpdated = session.getUpdatedOn();
		long now = System.currentTimeMillis() / 1000;
		Date lastLogin = new Date(session.getCreatedOn() * 1000);
		Format format = new SimpleDateFormat(Utils.DATE_FORMAT_LONG);
		String loggedOn = format.format(lastLogin);
		int ago = Math.toIntExact(now - lastUpdated);
		log.info("<touchSession>: User '" + session.getUserId() + "' updated session "
				+ ago + " seconds ago. Logged on '" + loggedOn + "'");


		// Add callStack so we can trace where we're spending time...
		Throwable t = new Throwable();
		StackTraceElement[] elements = t.getStackTrace();
		//String calleeMethod = elements[0].getMethodName();
		String stackTrace = "";
		for (int i = 1; i <= 5; i++) {
			String callerMethodName = elements[i].getMethodName();
			String callerClassName = elements[i].getClassName();
			stackTrace += callerClassName + "." + callerMethodName + "()" + " <-- ";
		}
		stackTrace = stackTrace.substring(0, stackTrace.length() - 4);
		log.debug("<touchSession>: from " + stackTrace);
	}


	/**
	 * As there exists 2 methods for accessing a uuid (via Admin/Main), we will use a
	 * single <code>getUserIdentity</code> and base the rest of our code on <b>userId</b>.
	 * 
	 * @param request ...
	 * @return String ...
	 */
	private String getUserIdentity(HttpServletRequest request) {
		String userId = getAttribute(request, Constants.USER_ID);
		if (userId == null || userId.equals("")) {
			// In case it is an admin request....
			userId = getAttribute(request, Constants.USERNAME);
			try {
				userId = userId.toLowerCase();
			} catch (Exception ignore) {}
		}
		return userId;
	}

	/**
	 * Checks if <code>request</code> comes from an Admin perspective. 
	 * <p>
	 * More precisely, as admin will be isolated from Moodle, this method will return <code>true</code>
	 * if <code>request</code> doesn't have a token checksum, which is what is used as a simple
	 * authorization/validation method for mere mortals.
	 *  
	 * @param request ...
	 * @return boolean ...
	 */
	public boolean isAdminRequest(HttpServletRequest request) {
		return getAttribute(request, Constants.TOKEN) == null;
	}
	

	
	/**
	 * Consolidates two different session attributes into a single serialized String...
	 * 
	 * @param oldAttribs ...
	 * @param newAttribs ...
	 * @return A merged String containing attributes from both parameters
	 */
	private String consolidateAttributes(String oldAttribs, String newAttribs) {
		HashMap<String, String> mapOld = new HashMap<>();
	    String[] pairs = oldAttribs.split("&");
	    for (String pair : pairs) {
	        int idx = pair.indexOf("=");
	        try {
	        	mapOld.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
	        } catch (Exception ignore) {}
	    }
	    HashMap<String, String> mapNew = new HashMap<>();
	    pairs = newAttribs.split("&");
	    for (String pair : pairs) {
	        int idx = pair.indexOf("=");
	        try {
	        	mapNew.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
	        } catch (Exception ignore) {}
	    }
	    
	    // Merge both attribs in one single Map and serialize...
	    HashMap<String, String> resultMap = new HashMap<>();
	    resultMap.putAll(mapOld);
	    resultMap.putAll(mapNew);
	    String result = "";
	    for (String key : resultMap.keySet()) {
	    	result += key + "=" + resultMap.get(key) + "&";
	    }
	    return result.substring(0, result.length() - 1); // throw away last &
	}
	


	/**
	 * Wrapper to query attributes|parameters from a <code>HttpServletRequest</code>
	 * object, so we can overload request with Attributes instead of base parameters...
	 *
	 * @param request ...
	 * @param attribute ...
	 */
	private String getAttribute(HttpServletRequest request, String attribute) {
		Object attr = request.getAttribute(attribute);
		if (attr != null && attr instanceof String) {
			return (String) attr;
		} else {
			return request.getParameter(attribute);
		}
	}

	/**
	 * Gets a String with all attributes of a <code>HttpServletRequest</code> object,
	 * separated by & and in key=value form.
	 * Includes both queryString parameters as well as Attributes (String) objects
	 *
	 * @param request ...
	 * @return String in the form key=value&key2=value2&...
	 */
	private String getAttributes(HttpServletRequest request) {
		List<String> listAttribs = new ArrayList<>();
		String attributes = request.getQueryString();
		if (attributes == null) attributes = "";
		// asList is immutable!
		listAttribs = new ArrayList<>(Arrays.asList(attributes.split("&")));

		// Overload with HttpServletRequest attributes
		Enumeration attrs =  request.getAttributeNames();
		while (attrs.hasMoreElements()) {
			String key = (String) attrs.nextElement();
			Object attr = request.getAttribute(key);
			if (attr != null && attr instanceof String) {
				String val = key + "=" + attr.toString();
				if (!listAttribs.contains(val)) {
					listAttribs.add(val);
				}
			}
		}

		// Serialize to store under session table...
		attributes = String.join("&", listAttribs);
		return attributes;
	}

	/**
	 * As a means of easy validation....
	 *
	 * @param id ...
	 * @param token ...
	 */
    public void validateToken(String id, String token) throws AuthException {
		String sha1sum = Utils.getSHA1(id + Config.getSecuritySalt());
		System.out.println("sha1sum = " + sha1sum + " -- " + id + Config.getSecuritySalt());
		if (!sha1sum.equals(token)) {
			throw new AuthException(i18n.getString(Config.getDefaultLocale(),
					"auth.invalid.checksum", token));
		}
    }

}