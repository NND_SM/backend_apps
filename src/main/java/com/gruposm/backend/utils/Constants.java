package com.gruposm.backend.utils;

/**
 * Constants class. 
 * <p>
 * All parameters, IDs, .... should be accesed solely via this class...
 * 
 * @version 0.1
 * @date Jul 04, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class Constants
{
	/** Global attributes to be used here & in front-end servlets */
	public static final String UUID = "uuid";
    public static final String USER_ID = "user_id";
	public static final String USER_ROLE = "user_role";
	public static final String TIMESTAMP = "timestamp";
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String FNAME = "fname";
	public static final String LNAME = "lname";
	public static final String LOCALE = "locale";
	public static final String PICTURE = "picture";

	/** Token - Integrity checksum - Security */
	public static final String TOKEN = "token";
	public static final String TIMESTAMP_SHORT = "ts";


	/** Google Analytics */
	public static final String GOOGLE_ANALYTICS = "gaAccountId";

	/** MultiTenancy */
	public static final String TENANT = "tenant";

	/** Control de errores en cliente */
    public static final String JS_FATAL_ERRORS_ARRAY = "js_fatal_errors_array";
	public static final String JS_FATAL_ERRORS_REPORT_ALL = "js_fatal_errors_report_all";

	/** Firebase Utils */
	public static final String FROM_FIREBASE = "from_firebase";
    public static final String DEVICE_ID = "device_id";

    // Expirations types
    public static final String EXPIRATION_TYPE_NEVER = "never";
    public static final String EXPIRATION_TYPE_PERIOD = "period";
	public static final String EXPIRATION_TYPE_ONE_MONTH = "one_month";
	public static final String EXPIRATION_TYPE_TWO_MONTHS = "two_months";
	public static final String EXPIRATION_TYPE_THREE_MONTHS = "three_months";
	public static final String EXPIRATION_TYPE_SIX_MONTHS = "six_months";
	public static final String EXPIRATION_TYPE_ONE_YEAR = "one_year";


	public static final String SESSION_NAME = "backend-apps"; // anual


}