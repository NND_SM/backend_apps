package com.gruposm.backend.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.helpers.Loader;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * Clase de acceso al fichero de configuracion e inicializaciones estándar.
 * <p>
 * Permite el acceso a los paramteros de la aplicacion. El primero en llamarla
 * la inicializa y al ser estática se mantiene en memoria mientras no se
 * reinicie la aplicación.
 * <p>
 * Si no se indican nada, el fichero a buscar es el determinado por el atributo
 * <b>DEFAULT_PROPERTIES_FILE</b>. Si se desea usar un fichero de propiedades
 * diferente, se ha de llamar antes de todo a setPropertiesFile("NAME"). Hay que
 * tener en cuenta que esta clase es estática, con lo cual la llamada a
 * setPropertiesFile("NAME") sólo tendrá efecto la primera vez.
 * <p>
 * La búsqueda del fichero de propiedades sigue el siguiente patrón:
 * <p>
 * <ul>
 * <li>Primero se busca en la ruta indicada por las variable de sistema
 * <code>'user.dir'
 * </code>. Si dicha variable no existiera o la ruta indicada en ella no
 * contubiera el fichero indicado en <b>DEFAULT_POPERTIES_FILE</b> en dicha
 * carpeta o en la subcarpeta <code>config</config> se continuará buscando.</li>
 * <li>Segundo, se busca en el classpath a través de los diferentes classloaders 
 * gracias a la ayuda de la clase <code>'Loader'</code> de Log4Java.</li>
 * </ul>
 * 
 * Una vez encontrado el fichero, logamos la ubización del mismo tanto a Log4J
 * como a System.out para evitar confusiones en caso se que en la jerarquía de
 * carpetas existiera más de un fichero de propiedades.
 * <p>
 * La inicialización de Log4J ya no es necesaria. Se hace automaticamente
 * siempre que exista un fichero <code>log4j.xml</code> en el classpath.
 * <p>
 * 
 * @author Juan Herrera
 * @author Jordi G. March
 * @date Hace mucho ya...
 */
public class Settings {
	/** Log4Java */
	static private Logger log = Logger.getLogger(Settings.class);

	/** -Duser.dir environmental variable */
	private static final String USER_DIR = "user.dir";

	/**
	 * Nombre del fichero de propiedades por defecto en caso de no suministrar
	 * ninguno...
	 */
	private static String DEFAULT_PROPERTIES_FILE = "backend.properties";

	/**
	 * Directorio por defecto donde se ha encontrado la configuración
	 */
	private static String CONFIG_DIR;

	/** Atributos que mantendrán las propiedades */
	private static Properties properties = new Properties();

	static {
		initialize();
	}
	
	private static void initialize() {
		log.info("<initialize>: Inicializando la carga del fichero de propiedades: " + DEFAULT_PROPERTIES_FILE);
		String propertiesPath = getPropertiesPath();
		// Actualizo mi directorio por defecto encontrado tras propertiesPath...
		CONFIG_DIR = propertiesPath.replace(DEFAULT_PROPERTIES_FILE, "");

		try {
			FileInputStream fis = new FileInputStream(propertiesPath);
			properties.load(fis);
			fis.close();

			log.info("<initProperties>: Cargado con éxito el fichero de propiedades '" + DEFAULT_PROPERTIES_FILE + "'");
		} catch (Exception e) {
			log.fatal("<initialize>: Error desconocido al cargar el fichero de propiedades '" + DEFAULT_PROPERTIES_FILE
					+ "': " + e.toString());
			/* TODO: Levantar a la clase de gestión de errores */
		}
	}

	/**
	 * Búsqueda de PATH absoluto del directorio contenedor de los ficheros de
	 * propiedades a través de la variable de entorno <code>user.dir</code>
	 * 
	 * @return String con el PATH absoluto del sistema de ficheros local.
	 */
	private static String searchUserDir() {
		// Primero buscamos con user_dir estándar de sistema...
		String searchPath = System.getProperty(USER_DIR);

		if (searchPath == null || searchPath.equals("")) {
			log.warn("<searchUserDir>: No se ha especificado la ruta al fichero de propiedades "
					+ "utilizando la variable de entorno -Duser.dir");
		} else {
			if (!searchPath.substring(searchPath.length() - 1).equals(File.separator))
				searchPath += File.separator;

			// Primero buscamos a partir de user_dir estándar...
			File f = new File(searchPath + DEFAULT_PROPERTIES_FILE);
			if (f.exists()) {
				log.info("<searchUserDir>: Encontrado fichero de propiedades vía user_dir, " + "en la ruta absoluta '"
						+ searchPath + DEFAULT_PROPERTIES_FILE + "'.");
				return searchPath + DEFAULT_PROPERTIES_FILE;
			} else {
				// ... y a partir de user_dir/config como fallback...
				f = new File(searchPath + "config" + File.separator + DEFAULT_PROPERTIES_FILE);
				if (f.exists()) {
					log.info("<searchUserDir>: Encontrado fichero de propiedades vía user_dir, "
							+ "en la ruta absoluta '" + searchPath + "config" + File.separator + DEFAULT_PROPERTIES_FILE
							+ "'.");
					return searchPath + "config" + File.separator + DEFAULT_PROPERTIES_FILE;
				}
			}
		}

		log.warn("<searchUserDir>: No se ha encontrado la ruta al fichero de propiedades "
				+ "utilizando la búsqueda vía variable de entorno 'user.dir'");
		return null;
	}

	/**
	 * Búsqueda de PATH absoluto del directorio contenedor de los ficheros de
	 * propiedades a través de la jerarquía de clases...
	 * 
	 * @return String con el PATH absoluto del sistema de ficheros local.
	 */
	private static String searchClassLoader() {
		if (Loader.getResource(DEFAULT_PROPERTIES_FILE) != null) {
			String searchPath = Loader.getResource(DEFAULT_PROPERTIES_FILE).getFile();
			log.info("<searchClassLoader>: Encontrado fichero de propiedades vía ClassLoader, "
					+ "en la ruta absoluta '" + searchPath + "'.");
			return searchPath;
		}

		return null;
	}

	/**
	 * Ruta de búsqueda según las metodologías contempladas, a saber:
	 * <ul>
	 * <li>Primero se busca en la ruta indicada por las variable de sistema
	 * <code>'user.dir'
	 * </code>. Si dicha variable no existiera o la ruta indicada en ella no
	 * contubiera el fichero indicado en <b>DEFAULT_POPERTIES_FILE</b> en dicha
	 * carpeta o en la subcarpeta
	 * <code>config</config> se continuará buscando.</li>
	 * <li>Segundo, se busca en el classpath a través de los diferentes classloaders 
	 * gracias a la ayuda de la clase <code>'Loader'</code> de Log4Java.</li>
	 * </ul>
	 * 
	 * @return String con la ruta absoluta de búsqueda en el sistema de
	 *         ficheros.
	 */
	private static String getPropertiesPath() {
		String searchPath = "";

		// Seguimos según el orden acordado...
		searchPath = searchUserDir();
		if (searchPath == null || searchPath.equals("")) {
			log.warn("<getPropertiesPath>: Busqueda alternativa de los ficheros de "
					+ "propiedades a través de ClassLoader...");
			searchPath = searchClassLoader();
		}

		if (searchPath == null || searchPath.equals("")) {
			log.fatal("No se ha podido encontrar '" + DEFAULT_PROPERTIES_FILE + "' a "
					+ "través de ninguna de las rutas de búsqueda preestablecidas!");
			log.fatal("Abortando operación y notificando al administrador...");

			/*
			 * TODO: Levantar a la superclase de errores y terminar la
			 * aplicación
			 */
			throw new RuntimeException("TODO");
		} else
			return searchPath;

	}

	/**
	 * Fija el nombre del fichero de propiedades que se va a utilizar. Si no se
	 * suministra ninguno, se utiliza el valor por defecto definido en el
	 * atributo <code>DEFAULT_PROPERTIES_FILE</code>
	 * 
	 * @param propertiesFile Nombre del fichero de propiedades que se va a utilizar.
	 */
	public static void setPropertiesFile(String propertiesFile) {
		DEFAULT_PROPERTIES_FILE = propertiesFile;
	}

	/**
	 * Devuelve el valor de una clave del fichero de propiedades cargado.
	 * <p>
	 * Inicializa el sistema de carga del fichero en caso de que sea la primera
	 * llamada.
	 * 
	 * @param key Clave que queremos buscar
	 * @return ...
	 */
	public static String getSetting(String key) {
		return properties.getProperty(key);
	}

	/**
	 * Devuelve el valor de un fichero de propiedades a encontrar en la ruta absoluta
	 * de ficheros de configuración, según se haya encontrado previamente el fichero
	 * de propiedades principal.
	 *
	 * @param file Fichero a buscar...
	 * @return CONFIG_DIR + file
	 */
	public static String getConfigFile(String file) {
		return CONFIG_DIR + file;
	}

	/**
	 * TODO: implementar método de recarga que suponga el mínimo overhead
	 * posible...
	 * 
	 * @param key Clave que queremos buscar
	 * @return ...
	 */
	public static synchronized String reloadSetting(String key) {
		String new_value = "";

		log.info("<reloadSetting>:Recargando fichero de propiedades '" + DEFAULT_PROPERTIES_FILE + "'...");
		initialize();
		new_value = getSetting(key);
		log.info("<reloadSetting> Leído nuevo valor para '" + key + "': '" + new_value + "'");

		return new_value;
	}

}