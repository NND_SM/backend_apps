package com.gruposm.backend.utils;

import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Config class. 
 * <p>
 * All parameters/configuration should be accesed solely via this class...
 * 
 * @see !backend.properties
 * @version 0.1
 * @date Jul 04, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class Config
{
	/** Log4Java */
	private static final Logger log = Logger.getLogger(Config.class);

	/** Basic Host variables */
	private static final String BACKEND_PROTOCOL = "backend.protocol";
	private static final String BACKEND_HOST = "backend.host";
	private static final String BACKEND_PORT = "backend.port";
	private static final String BACKEND_WEBROOT = "backend.webroot";
	
	/** Basic Connection properties */
	private static final String DB_USER = "jdbc.username";
	private static final String DB_PASS = "jdbc.password";
	private static final String DB_URL = "jdbc.url";
	private static final String DB_DRIVER = "jdbc.driverClassName";
	private static final String DB_MAX_CONN = "jdbc.maxActiveConnections"; 
	private static final String DB_HIKARI_ENABLED = "jdbc.hikariEnabled";

	/** Token for administrative tasks */
	private static final String TOKEN_ADMIN_TASKS = "token.admin";
	
	/** Google Analytics */
	private static final String GA_ENABLED = "ga.enabled";
	private static final String GA_ACCOUNT_ID = "ga.accountid";
	
	/** MyBatis Cache enabled? */
	private static final String MYBATIS_CACHE_ENABLED = "mybatis.cache.enabled";

	/** Different Resource Handlers */
	private static final String WEB_PREFIX_PATH = "web.prefix.path";
	private static final String WEB_MAIN_PATH = "web.main.path";
	private static final String WEB_ADMIN_PATH = "web.admin.path";

	/** Directory Listing Allowed - Static Context Handler */
	private static final String WEB_DIR_LISTING_ALLOWED = "web.dir.listing.allowed";

	/** Security Settings */
	private static final String SECURITY_SESSION_REQUIRED = "security.session.required";
	private static final String SECURITY_SESSION_TIMEOUT = "security.session.timeout.mins";
	private static final String SECURITY_CHECKSUM_SALT = "security.checksum.salt";
	private static final String SECURITY_CHECKSUM_TIMEOUT_ENABLED = "security.checksum.timeout.enabled";
	private static final String SECURITY_CHECKSUM_TIMEOUT = "security.checksum.timeout.mins";

	/** Assets - JSON versions and the like... */
	private static final String ASSETS_PATH = "assets.path";
	private static final String ASSETS_JSON_PATH = "assets.json.path";
	private static final String ASSETS_DEFAULT_JSON_FILE = "assets.default.json.file";

	/** Default Locale */
	private static final String LOCALE_DEFAULT = "locale.default";
	
	/** Template Caching? Use only in production */
	private static final String CONFIG_TEMPLATE_CACHING = "config.template.caching";

	/** Defaults to http:// */
	public static String getBackendProtocol() {
		return Settings.getSetting(BACKEND_PROTOCOL);
	}
	/** Defaults to localhost/0.0.0.0 */
	public static String getBackendHost() {
		return Settings.getSetting(BACKEND_HOST);
	}
	/** Defaults to 8888 */
	public static String getBackendPort() {
		return Settings.getSetting(BACKEND_PORT);
	}
	/** Defaults to / -- IMPORTANT: ALWAYS FINISH WITH SLASH! */
	public static String getBackendWebroot() {
		return Settings.getSetting(BACKEND_WEBROOT);
	}
	
	/** Defaults to root */
	public static String getDbUser() {
		return Settings.getSetting(DB_USER);
	}
	/** Defaults to root */
	public static String getDbPass() {
		return Settings.getSetting(DB_PASS);
	}
	/** Defaults to jdbc:mysql://localhost:3306/backend */
	public static String getDbUrl() {
		return Settings.getSetting(DB_URL);
	}
	/** Defaults to com.mysql.jdbc.Driver */
	public static String getDbDriver() {
		return Settings.getSetting(DB_DRIVER);
	}
	/** Defaults to 50 */
	public static int getDbMaxConnections() {
		int numConn = 30;
		try {
			numConn = Integer.parseInt(Settings.getSetting(DB_MAX_CONN));
		} catch (Exception ignore) {}
		return numConn;
	}
	/** Defaults to true */
	public static boolean isHikariCPEnabled() {
		return Settings.getSetting(DB_HIKARI_ENABLED).equalsIgnoreCase("true");
	}

	/** Defaults to f2b92a816ddf0f8067f5ed44f881e4cf5d3ef9gk */
	public static String getAdminToken() {
		return Settings.getSetting(TOKEN_ADMIN_TASKS);
	}
	
	/** Defaults to true */
	public static boolean isGAEnabled() {
		return Settings.getSetting(GA_ENABLED).equalsIgnoreCase("true");
	}
	/** Defaults to UA-TEST328-1 */
	public static String getGAaccountId() {
		return Settings.getSetting(GA_ACCOUNT_ID);
	}
	
	/** Defaults to false */
	public static boolean isMyBatisCacheEnabled() {
		return Settings.getSetting(MYBATIS_CACHE_ENABLED).equalsIgnoreCase("true");
	}

	/** Defaults to current dir of execution, followed by a trailing slash! */
	public static String getStaticAppPath() {
		return System.getProperty("user.dir") + "/";
	}

	/** Defaults to false */
	public static boolean isDirListingAllowed() {
		return Settings.getSetting(WEB_DIR_LISTING_ALLOWED).equalsIgnoreCase("true");
	}

	/** Defaults to web/ */
	public static String getWebPrefixPath() {
		return Settings.getSetting(WEB_PREFIX_PATH);
	}
	/** Defaults to dist/ */
	public static String getWebMainPath() {
		return Settings.getSetting(WEB_MAIN_PATH);
	}
	/** Defaults to admin/ */
	public static String getWebAdminPath() {
		return Settings.getSetting(WEB_ADMIN_PATH);
	}
	
	/** Defaults to true */
	public static boolean isSessionRequired() {
		return Settings.getSetting(SECURITY_SESSION_REQUIRED).equalsIgnoreCase("true");
	}
	/** Defaults to 480 minutes */
	public static int getSessionTimeout() {
		int minutes = 30;
		try {
			minutes = new Integer(Settings.getSetting(SECURITY_SESSION_TIMEOUT));
		} catch (Exception ignore) {}
		if (minutes <= 0) {
			minutes = Integer.MAX_VALUE / 1000; // Para permitir margen de * 60 y tal... 
		}
		return minutes;
	}
	/** Defaults to .... (a typical salt) */
	public static String getSecuritySalt() {
		return Settings.getSetting(SECURITY_CHECKSUM_SALT);
	}
	/** Security Checksum timeout enabled? Caducidad de token... Por defecto false */
	public static boolean isSecurityChecksumTimeoutEnabled() {
		return Settings.getSetting(SECURITY_CHECKSUM_TIMEOUT_ENABLED).equalsIgnoreCase("true");
	}
	/** Defaults to 2 hours - In seconds! */
	public static int getSecurityChecksumTimeout() {
		int minutes = 120;
		try {
			minutes = new Integer(Settings.getSetting(SECURITY_CHECKSUM_TIMEOUT));
		} catch (Exception ignore) {}
		if (minutes <= 0) {
			minutes = Integer.MAX_VALUE / 1000; // No hay timeout...
		}
		return minutes * 60;
	}

	/** Defaults to false */
	public static boolean getThymeleafCache() {
		return Settings.getSetting(CONFIG_TEMPLATE_CACHING).equalsIgnoreCase("true");
	}

	/** Defaults to es|en|... */
	public static String getDefaultLocale() {
		return Settings.getSetting(LOCALE_DEFAULT);
	}

	/** Defaults to true... */
	public static boolean isTokenSecurityEnabled() {
		return false;
	}

	/** Defaults to assets/ */
	public static String getAssetsPath() {
		String assetsPath = Settings.getSetting(ASSETS_PATH);
		File finalPath;
		if (assetsPath == null || assetsPath.equals("")) {
			assetsPath = "assets/";
		}
		if (!assetsPath.endsWith("/")) assetsPath += "/";

		// Creamos la ruta en caso de no existir
		finalPath = new File(getStaticAppPath() + assetsPath);
		if (!finalPath.exists()) {
			log.info("<getAssetsPath>: Creating " + finalPath.getAbsolutePath() + " assets directory...");
			finalPath.mkdir();
		}
		// Devolvemos nuestro assets/ inicial (assetsPath)
		return assetsPath;
	}
	/** Defaults to assets/json/ */
	public static String getAssetsJsonPath() {
		String assetsJsonPath = Settings.getSetting(ASSETS_JSON_PATH);
		if (assetsJsonPath == null || assetsJsonPath.equals("")) {
			assetsJsonPath = "assets/json/";
		}
		if (!assetsJsonPath.endsWith("/")) assetsJsonPath += "/";

		// Devolvemos nuestro assets/json/ inicial (assetsPath)
		return assetsJsonPath;
	}

	/** Defaults to language.json */
	public static String getDefaultAssetsJsonFile() {
		return Settings.getSetting(ASSETS_DEFAULT_JSON_FILE);
	}
}