package com.gruposm.backend.utils.thymeleaf;

import com.gruposm.backend.domain.session.Session;
import com.gruposm.backend.utils.AuthUtils;
import com.gruposm.backend.utils.Config;
import com.gruposm.backend.utils.Constants;
import com.gruposm.backend.utils.Utils;
import org.apache.log4j.Logger;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.messageresolver.StandardMessageResolver;

import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * General utils for i18n / Locale settings, in order to be able to use the same properties file
 * both for the end user front-end + back-end.
 * 
 * @version 0.1
 * @date Feb 01, 2017
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class LocaleUtils {

	/** Log4Java */
	private static final Logger log = Logger.getLogger(LocaleUtils.class);

	/** Singleton */
	private static final LocaleUtils instance;
	static {
		instance = new LocaleUtils();
	}

	/** Properties hashMap for easy/fast access */
	private static final ConcurrentHashMap<String, Properties> localeProperties = new ConcurrentHashMap<>();
	

	
	/**
	 * 
	 */
	public LocaleUtils() {
	}

	public static LocaleUtils getInstance() {
		return instance;
	}
	
	
	/**
	 * Adapts Thymeleaf Engine.StandardMessageResolver to check messages from a new source/language
	 * <p>
     * Overrides default Thymeleaf package Message Resolver so we can easily wrap all application messages
     * in a single file <code>messages_{locale}.properties</code>.
	 * 
	 * @param engine	Thymeleaf Template Engine in use
	 * @param language	Equivalent to request.getLocale().getLanguage() (short language == es|en|pt|...)
	 */
	public void adaptDefaultMessages(TemplateEngine engine, String language) {
    	// Get default StandardMessageResolver....
    	StandardMessageResolver mesRes = (StandardMessageResolver) engine.getMessageResolvers().iterator().next();
    	Properties props = getPropertiesFile(language);
    	mesRes.setDefaultMessages(props);		
	}
	
	
	/**
	 * Get properties file for a given language
	 * <p>
     * Uses an internal static HashMap in order to accelerate message properties fetching...
     * 
     * @param language Default short language (equiv. to Locale.getLanguage())
     */
	public Properties getPropertiesFile(String language) {
		Properties props = localeProperties.get(language);
		// Allow JRebel reloading while configuring (security.session.required must be false)
    	if (!Config.isSessionRequired() || props == null) {  // OJO LazyLoading!!!
    		props = new Properties();
    		String propertiesFile = "/i18n/messages_" + language + ".properties";
	    	try {
	    		props.load(getClass().getResourceAsStream(propertiesFile));
				localeProperties.put(language, props);
	    	} catch (Exception e) {
	    		log.warn("<getPropertiesFile>: Error loading properties for " + language);
	    		log.warn("<getPropertiesFile>: " + e.toString());
				log.warn("<getPropertiesFile>: Switching to default locale...");
	    		propertiesFile = "/i18n/messages.properties";
	    		try {
	    			props.load(getClass().getResourceAsStream(propertiesFile));
	    		} catch (Exception e2) {
	    			log.fatal("<initializeI18n>: Error loading default messages.properties!: " + e2.toString());
	    		}
	    	}
    	}
    	return props;
	}


	
	/**
	 * Gets the proper value associated to this <code>key</code>, using the supplied Locale...
	 * 
	 * @param locale
	 * @param key
	 * @return
	 */
	public String getString(Locale locale, String key) {
		return getString(locale.getLanguage(), key);
	}
	
	/**
	 * Gets the proper value associated to this <code>key</code>, using the supplied Locale, and 
	 * resolving the different parameters within the selected value.
	 * <p>
	 * Parameters should be in {0}, {1}, ... format at the properties level.
	 * 
	 * @param locale
	 * @param key
	 * @return
	 */
	public String getString(Locale locale, String key, String... params) {
		return getString(locale.getLanguage(), key, params);
	}
	
	/**
	 * Gets the proper value associated to this <code>key</code>, using the supplied _String_ locale...
	 * 
	 * @param locale
	 * @param key
	 * @return
	 */
	public String getString(String locale, String key) {
		Properties props = getPropertiesFile(locale);
		return props.getProperty(key);
	}

	/**
	 * Gets the proper value associated to this <code>key</code>, using the supplied _String_ locale, and 
	 * resolving the different parameters within the selected value.
	 * <p>
	 * Parameters should be in {0}, {1}, ... format at the properties level.
	 * 
	 * @param locale
	 * @param key
	 * @return
	 */
	public String getString(String locale, String key, String... params) {
		Properties props = getPropertiesFile(locale);
		return replaceParams(props.getProperty(key), params);
	}
	
	/**
	 * Replace parameters of a given String following this convention:
	 * <ul>
	 * <li>{0} in string will be replaced by first parameter</li>
	 * <li>{1} in string will be replaced by second parameter</li>
	 * <li>... and so on...</li>
	 * </ul>
	 * 
	 * @param value
	 * @param params String... to be replaced
	 * @return
	 */
	private String replaceParams(String value, String... params) {
		int i = 0;
		String result = value;
		for (String param : params) {
			result = result.replace("{" + i + "}", param);
			++i;
		}
		return result;
	}

}