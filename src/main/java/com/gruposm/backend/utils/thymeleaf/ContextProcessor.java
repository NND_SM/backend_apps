package com.gruposm.backend.utils.thymeleaf;

import com.gruposm.backend.utils.Config;
import com.gruposm.backend.utils.Constants;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.WebContext;

import java.util.HashMap;

/**
 * A generic place to pre-process all thymeleaf operations, be it online or for offline use.
 * <p>
 * There's one problem, though:
 * <ul>
 * <li>We use two different contexts for Online|Offline usage: WebContext && Context</li>
 * <li>These Contexts cannot be cast one-to-one or whatever, so we must duplicate methods for 
 * Online and Offline use :(</li>
 * <ul>
 * 
 * @version 0.1
 * @date Mar 9, 2017
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class ContextProcessor {

	/** Log4Java */
	// private static final Logger log = Logger.getLogger(ContextProcessor.class);

	/** Context Variables */
	private static final HashMap<String, Object> mapContext;
	
	/** Singleton */
	private static final ContextProcessor instance;
	static {
		instance = new ContextProcessor();
		mapContext = new HashMap<>();
	}
	
	public ContextProcessor() {
		
	}
	public static ContextProcessor getInstance() {
		return instance;
	}
	
	/**
	 * Build Common Thymeleaf variables...
	 */
	public static void buildCommonVars(String webRoot) {
		// Analytics
		if (Config.isGAEnabled()) {
			mapContext.put(Constants.GOOGLE_ANALYTICS, Config.getGAaccountId());
		}

	}

	/**
	 * Online Usage - Need WebContext & sessionId
	 * 
	 * @param ctx
	 * @param sessionId
	 */
	public void processBaseVariables(WebContext ctx, String sessionId) {
		
		// WebRoot - Relative for Web OnLine Usage...
		buildCommonVars(Config.getBackendWebroot());
				
		// Standard common variables...
		for (String key : mapContext.keySet()) {
			ctx.setVariable(key, mapContext.get(key));
		}
    }
	
	/**
	 * Offline Usage - No sessionId - OFFLINE appended at <code>mapper_sm_session_id</code>
	 * 
	 * @param ctx
	 */
	public void processBaseVariables(Context ctx, String FQDN) {
		
		// WebRoot - FQDN for OffLine Usage...
		if (!FQDN.endsWith("/")) FQDN += "/";
		buildCommonVars(FQDN + Config.getBackendWebroot());
		
    	// Standard common variables...
		for (String key : mapContext.keySet()) {
			ctx.setVariable(key, mapContext.get(key));
		}

	}
	
	/**
	 * Online Usage - mapAttribs that must be supplied via a valid sessionId
	 * 
	 * @param ctx
	 * @param mapAttribs
	 */
	public void processUserAttribs(WebContext ctx, HashMap<String, String> mapAttribs) {
		// Main Attribs...
		if (mapAttribs != null) {
			String uuid = mapAttribs.get(Constants.UUID);
			String userId = mapAttribs.get(Constants.USER_ID);
			String userRole = mapAttribs.get(Constants.USER_ROLE);
			long timestamp = System.currentTimeMillis() / 1000;
			try {
				timestamp = new Long(mapAttribs.get(Constants.TIMESTAMP)).longValue();
			} catch (Exception ignore) {}
			String fname = mapAttribs.get(Constants.FNAME);
			String lname = mapAttribs.get(Constants.LNAME);
			ctx.setVariable(Constants.UUID, uuid != null ? uuid : "uuid");
			ctx.setVariable(Constants.USER_ID, userId != null ? userId.toLowerCase() : "userId");
			ctx.setVariable(Constants.USER_ROLE, userRole != null ? userRole.toLowerCase() : "userRole");
			ctx.setVariable(Constants.TIMESTAMP, timestamp);
			ctx.setVariable(Constants.FNAME, fname != null ? fname : "");
			ctx.setVariable(Constants.LNAME, lname != null ? lname : userId);
		}
	}
	
	
	/**
	 * Both for offline/online usage - i18n
	 * 
	 * @param engine
	 * @param language
	 */
	public void processLocale(TemplateEngine engine, String language) {
		LocaleUtils.getInstance().adaptDefaultMessages(engine, language);
	}
	
}