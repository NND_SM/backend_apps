package com.gruposm.backend.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.tika.Tika;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.mime.MimeType;

import java.io.File;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * General utils...
 * 
 * @version 0.1
 * @date Jun 24, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class Utils {

	/** Log4Java */
	private static final Logger log = Logger.getLogger(Utils.class);

	/** Default Date Long Format */
	public static final String DATE_FORMAT_LONG = "dd/MM/yyyy HH:mm:ss";
	public static final String DATE_FORMAT_SHORT = "dd/MM/yyyy";
	public static final String DATE_FORMAT_ISO8601 = "yyyy/MM/dd HH:mm:ss";

	/** Singleton */
	private static final Utils instance;
	static {
		instance = new Utils();
	}

	/**
	 * 
	 */
	public Utils() {
	}

	public static Utils getInstance() {
		return instance;
	}


	/**
	 * Get the SHA-1 representation of the given String
	 */
	public static String getSHA1(String string) {
		return DigestUtils.sha1Hex(string);
	}

	/**
	 * 
	 * @param text
	 *            to clean/remove illegal chars...
	 * @return String
	 */
	public static String cleanChars(String text) {
		String newText = Normalizer.normalize(text, Normalizer.Form.NFD);
		newText = newText.replaceAll("[^\\p{ASCII}]", "");
		newText = newText.replaceAll("[^a-zA-Z0-9-_\\.]", "_");
		return newText;
	}

	/**
	 * Get a given parameter value from a standard web Query String...
	 * 
	 * @param queryString -
	 * @param param -
	 * @return String
	 */
	public static String getParamValue(String queryString, String param) {
		HashMap<String, String> params = getQueryMap(queryString);
		return params.get(param);
	}

	private static HashMap<String, String> getQueryMap(String query) {
		String[] params = query.split("&");
		HashMap<String, String> map = new HashMap<String, String>();
		for (String param : params) {
			String[] p = param.split("=");
			String name = p[0];
			if (p.length > 1) {
				String value = p[1];
				map.put(name, value);
			}
		}
		return map;
	}

	/**
	 * Redondeo a dos decimales (no existe por defecto en Math...)
	 * 
	 * @param number -
	 * @return int
	 */
	public static int roundZeroDecimals(double number) throws NumberFormatException {
		DecimalFormat zeroDForm = new DecimalFormat("#");
		return Integer.valueOf(zeroDForm.format(number));
	}

	/**
	 * Redondeo a dos decimales (no existe por defecto en Math...)
	 * 
	 * @param number -
	 * @return double
	 */
	public static double roundTwoDecimals(double number) throws NumberFormatException {
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		return Double.valueOf(twoDForm.format(number));
	}

	/**
     *
	 * Parse and return custom attribute (from extra fields, attribs, ...)
	 *
	 * @param attribs with attributes separated by &
	 * @param attrib to find
	 * @return Value
	 */
	public static String getCustomAttribute(String attribs, String attrib) {
		HashMap<String, String> mapAttributes = new HashMap<>();
		try {
			for (String pair : attribs.split("&")) {
				int idx = pair.indexOf("=");
				try {
					mapAttributes.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
				} catch (Exception ignore) {}
			}
			return mapAttributes.get(attrib);
		} catch (Exception ignore) {
			return "";
		}
	}

	/**
	 * Removes an attribute from a string of attributes... (from extra fields, attribs, ...)
	 *
	 * @param attribs with attributes separated by &
	 * @param attrib to find and remove ..
	 */
	public static String removeCustomAttribute(String attribs, String attrib) {
		HashMap<String, String> mapAttributes = new HashMap<>();
		try {
			for (String pair : attribs.split("&")) {
				int idx = pair.indexOf("=");
				try {
					String key = URLDecoder.decode(pair.substring(0, idx), "UTF-8");
					String value = URLDecoder.decode(pair.substring(idx + 1), "UTF-8");
					System.out.println("key = " + key + " -- value = " + value);
					if (!attrib.equals(key)) {
						System.out.println("Adding key " + key);
						mapAttributes.put(key, value);
					}
				} catch (Exception ignore) {}
			}
			// Regenerate attribs....
			Iterator it = mapAttributes.entrySet().iterator();
			StringBuilder params = new StringBuilder();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				params.append(pair.getKey()).append("=").append(pair.getValue()).append("&");
				it.remove(); // avoids a ConcurrentModificationException
			}
			attribs = params.toString();
			// Removes last &
			attribs = attribs.substring(0, attribs.length()-1);
			return attribs;
		} catch (Exception ignore) {
			return attribs;
		}
	}

	/**
	 * StackTrace al Log4Java...
	 *
	 * @param e Excepeción de turno...
	 */
	public static void logStrackTrace(Exception e) {
		log.error("----------  Begin StackTrace  --------------------------------");
		StackTraceElement[] stackTraceElements = e.getStackTrace();
		for (StackTraceElement stackTrace : stackTraceElements) {
			log.error(stackTrace.getClassName() + "  " + stackTrace.getMethodName()
					+ " " + stackTrace.getLineNumber());
		}
		log.error("----------  End StackTrace  ----------------------------------");
	}

}