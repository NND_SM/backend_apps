package com.gruposm.backend.utils;

import com.gruposm.backend.domain.AppCode;
import com.gruposm.backend.service.AppCodeService;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Utilidades para la generación de códigos...
 * 
 * @version 0.1
 * @date May 29, 2018
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class CodeUtils {

	/**
	 * Log4Java
	 */
	private static final Logger log = Logger.getLogger(CodeUtils.class);

	private static final CodeUtils instance;

	static {
		instance = new CodeUtils();
	}

	/**
	 *
	 */
	public CodeUtils() {
	}

	public static CodeUtils getInstance() {
		return instance;
	}

	/**
	 * Generates a random 6 digit code (caps), and checks against DDBB that the
	 * code returned has not been generated before.
	 *
	 * @return String
	 */
	private String genRandomCode() {
		long ini = System.currentTimeMillis();
		boolean found = false;
		String code = "";
		while (!found) {
			UUID uid = UUID.randomUUID();
			code = uid.toString().substring(0, 6).toUpperCase();
			if (code.contains("0") || code.toLowerCase().contains("o")) {
				// just ignore...
			} else {
				AppCodeService acs = new AppCodeService();
				found = acs.getBy(code) == null;
			}
		}
		long time = System.currentTimeMillis() - ini;
		log.info("<genRandomCode>: New code [" + code + "] generated in " + time + " ms.");
		return code;
	}


	private void insertAppCode(int idApp, String code, String expirationType) {
		AppCodeService acs = new AppCodeService();
		AppCode appCode = new AppCode();
		appCode.setIdApp(idApp);
		appCode.setUuidCode(code);
		appCode.setCreatedOn(System.currentTimeMillis());
		appCode.setExpirationType(expirationType);
		acs.insert(appCode);
	}



	/**
	 * Para generación bajo demanda según pidan Salva/MªJose....
	 */
	public void genAppsCodes(List<String>apps, Integer amountPerApps, String expirationType) {
		for (String appID : apps) {
			for (int j = 0; j < amountPerApps; j++) {
				String code = genRandomCode();
				insertAppCode(Integer.parseInt(appID), code, expirationType);
				System.out.println(code);
			}
			System.out.println("-----------------------------------------------------------");
		}
	}

	public static void main(String[] args) {
		// System.out.println("code = " + CodeUtils.getInstance().genRandomCode());
		List<String> apps = Arrays.asList("Artom", "Artric", "Arkim", "Arlant", "Arxy", "Crantal");
		CodeUtils.getInstance().genAppsCodes(apps, 2, null);
		System.exit(0);
	}

}