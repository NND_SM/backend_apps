package com.gruposm.backend.service;

import com.gruposm.backend.persistence.ConnectionFactory;
import com.gruposm.backend.persistence.mappers.DBCreatorMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

/**
 * Service Class for DataBase
 * 
 * @see package com.gruposm.backend.persistence.mappers
 * 
 * @version 0.1
 * @date Jun 27, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class DBCreatorService {
	
	/** Log4Java */
	private static final Logger log = Logger.getLogger(DBCreatorService.class);
	
	public void createDataBase() {
		SqlSession sqlSession = ConnectionFactory.getSession().openSession();
		DBCreatorMapper creatorMapper = null;
		try {
			creatorMapper = sqlSession.getMapper(DBCreatorMapper.class);
			// First - Disable all checks
			creatorMapper.disableChecks();
			// Tables....
			creatorMapper.createTenant();
			creatorMapper.createAdmin();
			creatorMapper.createApp();
			creatorMapper.createAppCode();
			log.info("<createDataBase>: Tables [...] updated successfully!");
		} finally {
			// Last - Enable checks again
			creatorMapper.enableChecks();
			sqlSession.commit();
			sqlSession.close();
		}
	}
	
	public void dropDataBase() {
		SqlSession sqlSession = ConnectionFactory.getSession().openSession();
		try {
			DBCreatorMapper creatorMapper = sqlSession.getMapper(DBCreatorMapper.class);
			// Tables....
			creatorMapper.dropAdmin();
			creatorMapper.dropTenant();
			creatorMapper.dropAppCode();
			creatorMapper.dropApp();
			log.info("<createDataBase>: Tables [...] dropped successfully!");
		} finally {
			sqlSession.commit();
			sqlSession.close();
		}
	}

	/**
	 * Cambios a popular en BBDD una vez hayamos salido en producción...
	 * <p>
	 * Aquí vamos metiendo todos los alters....
	 * <p>
	 *     OJO! Los metemos por separado para que no afecte a la atomicidad de cada
	 *     uno de ellos!!
	 * </p>
	 */
	public void performAlters() {

		// Add default Data
		addTenantData();
		addAdminData();

		log.info("<performAlters>: Tables [...] altered successfully!");
	}

	/**
	 * Adds default data that _MUST EXIST_ in every deployment!
	 *
	 * @date 21/Sep/2017
	 */
	private void addTenantData() {
		SqlSession sqlSession = ConnectionFactory.getSession().openSession();
		try {
			DBCreatorMapper creatorMapper = sqlSession.getMapper(DBCreatorMapper.class);
			creatorMapper.addTenantData();
		} catch (Exception ignore) {
		} finally {
			sqlSession.commit();
			sqlSession.close();
		}
	}

	/**
	 * Adds defaults admin that _MUST EXIST_ in every deployment!
	 *
	 * @date 21/Sep/2017
	 */
	private void addAdminData() {
		SqlSession sqlSession = ConnectionFactory.getSession().openSession();
		try {
			DBCreatorMapper creatorMapper = sqlSession.getMapper(DBCreatorMapper.class);
			creatorMapper.addAdminData();
		} catch (Exception ignore) {
		} finally {
			sqlSession.commit();
			sqlSession.close();
		}
	}

}