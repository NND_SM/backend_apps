package com.gruposm.backend.service;

import com.gruposm.backend.domain.App;
import com.gruposm.backend.persistence.mappers.AppMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionManager;

import java.util.List;

/**
 * Service Class for App administration
 * 
 * @version 0.1
 * @date Nov 15, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class AppService extends AbstractService {

	public AppService() {
		super();
	}
	public AppService(SqlSessionManager sessionManager) {
		super(sessionManager);
	}
	
	
	/**
	 * Default Insert method, based on <code>app</code>
	 * 
	 * @param app App object
	 */
	public void insert(App app) {
		SqlSession sqlSession = getSession();
		try {
			AppMapper appMapper = sqlSession.getMapper(AppMapper.class);
			appMapper.insert(app);
		} finally {
			closeSession(sqlSession);
		}
	}
	
	/**
	 * Update app....
	 * @param app ...
	 */
	public void update(App app) {
		SqlSession sqlSession = getSession();
		try {
			AppMapper appMapper = sqlSession.getMapper(AppMapper.class);
			appMapper.update(app);
		} finally {
			closeSession(sqlSession);
		}
	}
	
	/**
	 * Get All Apps
	 * 
	 */
	public List<App> getAll() {
		SqlSession sqlSession = getSession();
		try {
			AppMapper appMapper = sqlSession.getMapper(AppMapper.class);
			return appMapper.getAll();
		} finally {
			closeSession(sqlSession);
		}
	}
	
	/**
	 * Default...
	 */
	public void remove(App app) {
		SqlSession sqlSession = getSession();
		try {
			AppMapper appMapper = sqlSession.getMapper(AppMapper.class);
			appMapper.remove(app);
		} finally {
			closeSession(sqlSession);
		}
	}
	
}