package com.gruposm.backend.service;

import com.gruposm.backend.domain.AppCode;
import com.gruposm.backend.exception.*;
import com.gruposm.backend.persistence.mappers.AppCodeMapper;
import com.gruposm.backend.utils.Config;
import com.gruposm.backend.utils.Constants;
import com.gruposm.backend.utils.Utils;
import com.gruposm.backend.utils.thymeleaf.LocaleUtils;
import org.apache.commons.io.FileUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.nio.charset.Charset;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.time.LocalDate;


/**
 * Service Class for AppCodeCode administration
 * 
 * @version 0.1
 * @date Nov 15, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class AppCodeService extends AbstractService {

	/** Log4Java */
	private static final Logger log = Logger.getLogger(AppCodeService.class);

	public AppCodeService() {
		super();
	}
	public AppCodeService(SqlSessionManager sessionManager) {
		super(sessionManager);
	}
	
	
	/**
	 * Default Insert method, based on <code>appCode</code>
	 * 
	 * @param appCode AppCode object
	 */
	public void insert(AppCode appCode) {
		SqlSession sqlSession = getSession();
		try {
			AppCodeMapper appCodeMapper = sqlSession.getMapper(AppCodeMapper.class);
			appCodeMapper.insert(appCode);
		} finally {
			closeSession(sqlSession);
		}
	}
	
	/**
	 * Update appCode....
	 * @param appCode ...
	 */
	public void update(AppCode appCode) {
		SqlSession sqlSession = getSession();
		try {
			AppCodeMapper appCodeMapper = sqlSession.getMapper(AppCodeMapper.class);
			appCodeMapper.update(appCode);
		} finally {
			closeSession(sqlSession);
		}
	}
	
	/**
	 * Get All AppCodes
	 * 
	 */
	public List<AppCode> getAll() {
		SqlSession sqlSession = getSession();
		try {
			AppCodeMapper appCodeMapper = sqlSession.getMapper(AppCodeMapper.class);
			return appCodeMapper.getAll();
		} finally {
			closeSession(sqlSession);
		}
	}
	
	/**
	 * Default...
	 */
	public void remove(AppCode appCode) {
		SqlSession sqlSession = getSession();
		try {
			AppCodeMapper appCodeMapper = sqlSession.getMapper(AppCodeMapper.class);
			appCodeMapper.remove(appCode);
		} finally {
			closeSession(sqlSession);
		}
	}

	/**
	 *
	 * @param id ...
	 * @return ...
	 */
	public AppCode getBy(String id) {
		SqlSession sqlSession = getSession();
		try {
			AppCodeMapper appCodeMapper = sqlSession.getMapper(AppCodeMapper.class);
			return appCodeMapper.getBy(id);
		} finally {
			closeSession(sqlSession);
		}
	}

	/**
	 *
	 * @param uuid ...
	 * @return ...
	 */
	public AppCode getBy(String uuid, String userId) throws BackendException {
		SqlSession sqlSession = getSession();
		try {
			AppCodeMapper appCodeMapper = sqlSession.getMapper(AppCodeMapper.class);
			AppCode appCode = appCodeMapper.getBy(uuid);
			if (appCode == null) {
				throw new NotFoundException(LocaleUtils.getInstance().getString(
						Config.getDefaultLocale(), "auth.code.notfound"));
			} else {
				if (appCode.getUsedBy() != null && !appCode.getUsedBy().equals(userId)) {
					// Nada, lanzamos excepción en caso de que los usuarios sean distintos
					// y el código ya haya sido usado....
					throw new AlreadyUsedException(LocaleUtils.getInstance().getString(
							Config.getDefaultLocale(), "auth.code.already.used"));
				} else {
					return appCode;
				}
			}
		} finally {
			closeSession(sqlSession);
		}
	}

	/**
	 * Gets the last JSON under assets/ folder, specified by the given json name
	 *
	 * @param fileToRead ...
	 * @return JSON text ...
	 */
    public String getLastVersion(String fileToRead) throws BackendException {
    	try {
			File file = new File(Config.getAssetsJsonPath() + fileToRead);
			return FileUtils.readFileToString(file, Charset.defaultCharset());
		} catch (Exception e) {
    		log.error("<getLastVersion>: Error getting " + fileToRead + ": " + e.getMessage());
			Utils.logStrackTrace(e);
			throw new BackendException(e.getMessage());
		}
    }


	/**
	 *
	 * @param appCode ...
	 * @param userId ...
	 * @throws LimitException ...
	 * @throws ExpirationException ...
	 */
	public void activate(AppCode appCode, String userId)
			throws LimitException, ExpirationException {
		// Ïnit Expiration data on activate
		startExpiration(appCode);

    	// Validate Expiration
		checkExpiration(appCode);

		if (appCode.getActivationCount() >= appCode.getActivationLimit()) {
			throw new LimitException("The limit amount of activations has been exceeded");
		}

		// Set UseBy
		appCode.setUsedBy(userId);
		// Increment activations count
		appCode.setActivationCount(appCode.getActivationCount() + 1);
	}



	private void startExpiration(AppCode appCode) {
		LocalDate now = Instant.now().atZone(ZoneId.systemDefault()).toLocalDate();
		if (appCode.getUsedOn() == null) {
			switch (appCode.getExpirationType()) {
				case Constants.EXPIRATION_TYPE_ONE_MONTH:
					appCode.setExpirationStart(now.toEpochDay());
					appCode.setExpirationDate(now.plusMonths(1).toEpochDay());
					break;
				case Constants.EXPIRATION_TYPE_TWO_MONTHS:
					appCode.setExpirationStart(now.toEpochDay());
					appCode.setExpirationDate(now.plusMonths(2).toEpochDay());
					break;
				case Constants.EXPIRATION_TYPE_THREE_MONTHS:
					appCode.setExpirationStart(now.toEpochDay());
					appCode.setExpirationDate(now.plusMonths(3).toEpochDay());
					break;
				case Constants.EXPIRATION_TYPE_SIX_MONTHS:
					appCode.setExpirationStart(now.toEpochDay());
					appCode.setExpirationDate(now.plusMonths(6).toEpochDay());
					break;
				case Constants.EXPIRATION_TYPE_ONE_YEAR:
					appCode.setExpirationStart(now.toEpochDay());
					appCode.setExpirationDate(now.plusYears(1).toEpochDay());
					break;
			}
		}
	}


	public void checkExpiration(AppCode appCode)
			throws ExpirationException {

		if (!appCode.getExpirationType().equals(Constants.EXPIRATION_TYPE_NEVER)) {
			long now = Math.round(new Date().getTime() / 1000);
			if (appCode.getExpirationStart() == null) {
				throw new ExpirationException("{\"code\":100, \"expiration\":"  + appCode.toExpirationString() + ", \"description\":\"The license code is not active.\"}");
			}
			if (now < appCode.getExpirationStart()) {
				throw new ExpirationException("{\"code\":101, \"expiration\":"  + appCode.toExpirationString() + ", \"description\":\"The license code has not started yet.\"}");
			}
			if (now > appCode.getExpirationDate()) {
				throw new ExpirationException("{\"code\":102, \"expiration\":"  + appCode.toExpirationString() + ", \"description\":\"The license code is expired.\"}");
			}
		}
	}

}