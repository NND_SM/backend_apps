package com.gruposm.backend.service;

import com.gruposm.backend.domain.Tenant;
import com.gruposm.backend.persistence.mappers.TenantMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionManager;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Service Class for Multitenancy
 * 
 * @see package com.gruposm.template.persistence.mappers
 * 
 * @version 0.1
 * @date Sep 21, 2017
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class TenantService extends AbstractService {

	/** Log4Java */
	private static final Logger log = Logger.getLogger(TenantService.class);

	public TenantService() {
		super();
	}
	public TenantService(SqlSessionManager sessionManager) {
		super(sessionManager);
	}

	public List<Tenant> getAll() {
		SqlSession sqlSession = getSession();
		try {
			TenantMapper tenantMapper = sqlSession.getMapper(TenantMapper.class);
			return tenantMapper.getAll();
		} finally {
			closeSession(sqlSession);
		}
	}

	public Tenant getById(String tenant) {
		SqlSession sqlSession = getSession();
		try {
			TenantMapper tenantMapper = sqlSession.getMapper(TenantMapper.class);
			return tenantMapper.getById(tenant);
		} finally {
			closeSession(sqlSession);
		}
	}

	public Tenant getByDomain(String tenant) {
		SqlSession sqlSession = getSession();
		try {
			TenantMapper tenantMapper = sqlSession.getMapper(TenantMapper.class);
			return tenantMapper.getByDomain(tenant);
		} finally {
			closeSession(sqlSession);
		}
	}

}