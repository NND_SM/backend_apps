package com.gruposm.backend.service;

import org.apache.log4j.Logger;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;


/**
 * Controller Service, as a single entry-point for admin/background/monitoring
 * tasks...
 * 
 * @version 0.1
 * @date Jul 08, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class ControllerService {

	/** Log4Java */
	private static final Logger log = Logger.getLogger(ControllerService.class);

	/** Quartz Scheduler, so anyone can access it from outside... */
	protected static SchedulerFactory schedulerFactory;
	protected static Scheduler scheduler;

	/** Singleton */
	private static final ControllerService instance;
	static {
	    instance = new ControllerService();
	}
	
	/**
	 * 
	 */
	public ControllerService() {
		// Instantiates a Quartz Scheduler...
		try {
			schedulerFactory = new StdSchedulerFactory("com/gruposm/backend/service/quartz.properties");
		    scheduler = schedulerFactory.getScheduler();
		    scheduler.start();
		} catch (Exception e) {
			log.error("<ControllerService>: Error instanciando Scheduler Quartz! No se "
					+ "ejecutarán tareas de administración/background!!: " + e.toString());
		}
	}
	
	public static ControllerService getInstance() {
		return instance;
	}
	
	public Scheduler getScheduler() {
		return scheduler;
	}


}