package com.gruposm.backend.persistence;

import com.gruposm.backend.utils.Config;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.cache.Cache;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.session.SqlSessionManager;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.apache.ibatis.transaction.managed.ManagedTransactionFactory;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Properties;

/**
 * SQLSessionFactory factory...
 * 
 * @version 0.1
 * @date Jun 27, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class ConnectionFactory {

	/** Singleton */
	private static SqlSessionFactory sqlMapper;
	
	/** Log4Java */
	private static final Logger log = Logger.getLogger(ConnectionFactory.class);

	/** Mappers for MyBatis */
	private static final String MAPPERS_PACKAGE = "com.gruposm.backend.persistence.mappers";
	
	/** A very fast SQL to keep connections up & running */
	private static final String DEFAULT_PING_QUERY = "SELECT 1 FROM DUAL";
	
	/** Default connection timeout (in ms) - Usually 30 minutes */
	private static final int DEFAULT_TIMEOUT_MILLIS = 1800000;
	
	/** Alternative DataSource for Hikari Connection Pool */
	private static final String HIKARI_DATA_SOURCE = "com.mysql.jdbc.jdbc2.optional.MysqlDataSource";
	
	/** Connection Attributes */
	private static String user;
	private static String password;
	private static String dbURL;
	private static String dbDriver;
	private static int dbMaxConn;
			
	static {
		// Basic JDBC connection properties
		user = Config.getDbUser();
		password = Config.getDbPass();
		dbURL = Config.getDbUrl();
		dbDriver = Config.getDbDriver();
		dbMaxConn = Config.getDbMaxConnections();
		try {			
			if (Config.isHikariCPEnabled()) {
				initializeHikariPool();	
			} else {
				initializeMyBatisPool();
			}
		} catch (Exception e) {
			System.out.println("Error instanciando base de datos!");
			e.printStackTrace();
			System.exit(1);	
		}
	}
	
	/**
	 * Default MyBatis database pool...
	 * 
	 * @throws Exception
	 */
	private static void initializeMyBatisPool() throws Exception {
		PooledDataSource dataSource = new PooledDataSource(dbDriver, dbURL, user, password);
		Properties props = new Properties();
		props.put("cachePrepStmts", "true");
		props.put("prepStmtCacheSize", "250");
		props.put("prepStmtCacheSqlLimit", "2048");
		props.put("useServerPrepStmts", "true");
		dataSource.setDriverProperties(props);
		// Need to specify a poolPing behaviour to keep connections up&running
		// (autoReconnect=true doesn't work properly)
		dataSource.setPoolPingEnabled(true);
		dataSource.setPoolPingQuery(DEFAULT_PING_QUERY);
		dataSource.setPoolPingConnectionsNotUsedFor(DEFAULT_TIMEOUT_MILLIS);
		// Performance settings...
		dataSource.setPoolMaximumActiveConnections(dbMaxConn);
		
		TransactionFactory transactionFactory = new JdbcTransactionFactory();
		Environment environment = new Environment("prod", transactionFactory, dataSource);
		Configuration configuration = new Configuration(environment);
		sqlMapper = new SqlSessionFactoryBuilder().build(configuration);
		sqlMapper.getConfiguration().addMappers(MAPPERS_PACKAGE);
		// Map mySQL _ natural convention to std Java camelCase...
		sqlMapper.getConfiguration().setMapUnderscoreToCamelCase(true);
		
		// Cache Enabled? Should be OFF in case of a High Availability environment and NO
		// cache distribution mechanism...
		sqlMapper.getConfiguration().setCacheEnabled(Config.isMyBatisCacheEnabled());
	}
	
	/**
	 * Default MyBatis database pool...
	 * 
	 * @throws Exception
	 */
	private static void initializeHikariPool() throws Exception {
		HikariDataSource dataSource = new HikariDataSource();
        dataSource.setMaximumPoolSize(dbMaxConn);
        // Set minimumIdle to allow a higher maxPoolSize while keeping a reasonable idle value.
		// Otherwise, a fixed pool size will be created with MAX stated value.
        int minimumIdle = dbMaxConn > 10 ? 10 : dbMaxConn;
        dataSource.setMinimumIdle(minimumIdle);
       	dataSource.setDataSourceClassName(HIKARI_DATA_SOURCE);
        dataSource.addDataSourceProperty("url", dbURL);
        dataSource.addDataSourceProperty("user", user);
        dataSource.addDataSourceProperty("password", password);
        // @see https://github.com/brettwooldridge/HikariCP/wiki/MySQL-Configuration
        dataSource.addDataSourceProperty("cachePrepStmts", "true");
        dataSource.addDataSourceProperty("prepStmtCacheSize", "250");
        dataSource.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        dataSource.addDataSourceProperty("useServerPrepStmts", "true");
        
        // AutoReconnection - Prod settings...
        dataSource.setIdleTimeout(300000);
        dataSource.setMaxLifetime(600000);
        
		TransactionFactory transactionFactory = new ManagedTransactionFactory();
		Environment environment = new Environment("prod", transactionFactory, dataSource);
		Configuration configuration = new Configuration(environment);
		sqlMapper = new SqlSessionFactoryBuilder().build(configuration);
		sqlMapper.getConfiguration().addMappers(MAPPERS_PACKAGE);
		// Map mySQL _ natural convention to std Java camelCase...
		sqlMapper.getConfiguration().setMapUnderscoreToCamelCase(true);
		
		// Cache Enabled? Should be OFF in case of a High Availability environment and NO
		// cache distribution mechanism...
		sqlMapper.getConfiguration().setCacheEnabled(Config.isMyBatisCacheEnabled());
	}

	public static SqlSessionFactory getSession() {
		return sqlMapper;
	}
	
	public static SqlSessionManager getSessionManager() {
		return SqlSessionManager.newInstance(sqlMapper);
	}

	/**
	 * 
	 */
	public static void clearCache() {
		Configuration config = sqlMapper.getConfiguration();
		ArrayList<String> arrayMappers = new ArrayList<>();
		
		// Instead of creating automagically via reflection, just drop here whichever 
		// mappers we may want to flush manually...
		// For instance - won't flush DBCreatorMapper....
		arrayMappers.add(MAPPERS_PACKAGE + ".AttachmentMapper");
		arrayMappers.add(MAPPERS_PACKAGE + ".MainEntityMapper");
		arrayMappers.add(MAPPERS_PACKAGE + ".UserMapper");
		arrayMappers.add(MAPPERS_PACKAGE + ".CollaborationMapper");
		arrayMappers.add(MAPPERS_PACKAGE + ".AdminMapper");
		arrayMappers.add(MAPPERS_PACKAGE + ".TenantMapper");
		
		for (String mapper : arrayMappers) {
			Cache cache = config.getCache(mapper);
			if (cache != null) {
				log.info("<clearCache>: Cache cleared for " + mapper);
				cache.clear();
			}	
		}
	}
}