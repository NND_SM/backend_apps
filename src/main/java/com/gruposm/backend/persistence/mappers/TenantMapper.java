package com.gruposm.backend.persistence.mappers;

import com.gruposm.backend.domain.Tenant;

import java.util.List;

/**
 * MultiTenancy Mapper
 * 
 * @version 0.1
 * @date Sep 21, 2017
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public interface TenantMapper {

	/**
	 * Gets all available tenants...
	 * @return List<Tenant> ...
	 */
	List<Tenant> getAll();

	/**
	 * Get by tenant id|name
	 * @param tenant ...
	 * @return Tenant
	 */
	Tenant getById(String tenant);

	/**
	 * Get by tenant domain (toLower + like)
	 * @param domain (backend-co.conectasm.com, backend-do, backend-br, ...)
	 * @return Tenant
	 */
	Tenant getByDomain(String domain);

}
