package com.gruposm.backend.persistence.mappers;

import com.gruposm.backend.domain.AppCode;

import java.util.List;

/**
 * App Code Mapper
 * 
 * @version 0.1
 * @date Nov 14, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public interface AppCodeMapper {

	/**
	 * Default Insert method,  
	 * @param appCode ...
	 */
	void insert(AppCode appCode);
	
	/**
	 * Default Update method,  
	 * @param appCode ...
	 */
	void update(AppCode appCode);
	
	/**
	 * Default...
	 * @return List<AppCode>
	 */
	List<AppCode> getAll();
	
	/**
	 * Default...
	 */
	void remove(AppCode appCode);

	/**
	 *
	 * @param uuid
	 * @return AppCode
	 */
	AppCode getBy(String uuid);
}
