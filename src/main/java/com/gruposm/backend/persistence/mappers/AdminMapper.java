package com.gruposm.backend.persistence.mappers;

import com.gruposm.backend.domain.Admin;

import java.util.HashMap;
import java.util.List;

/**
 * Admin Mapper
 * 
 * @version 0.1
 * @date Nov 14, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public interface AdminMapper {

	/**
	 * Default Insert method,  
	 * @param admin ...
	 */
	public void insert(Admin admin);
	
	/**
	 * Default Update method,  
	 * @param admin ...
	 */
	public void update(Admin admin);

	/**
	 * Default...
	 * @return List<Admin>
	 */
	public List<Admin> getAll();
	
	/** 
	 * 
	 * @param map of username,tenant
	 * @return Admin
	 */
	public Admin getByName(HashMap<String, String> map);

	/**
	 * Default remove method,
	 * @param admin ...
	 */
	public void remove(Admin admin);
	
}
