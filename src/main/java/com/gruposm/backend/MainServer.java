package com.gruposm.backend;

import com.gruposm.backend.exception.AuthException;
import com.gruposm.backend.exception.UnauthorizedException;
import com.gruposm.backend.persistence.ConnectionFactory;
import com.gruposm.backend.service.DBCreatorService;
import com.gruposm.backend.utils.Config;
import com.gruposm.backend.utils.Constants;
import com.gruposm.backend.utils.thymeleaf.LocaleUtils;
import com.gruposm.backend.utils.thymeleaf.TemplateEngineUtil;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.server.handler.gzip.GzipHandler;
import org.eclipse.jetty.servlet.*;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.webapp.WebAppContext;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.servlet.ServletContainer;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.EnumSet;


/**
 * Main class - Entry point
 * 
 * @version 0.1
 * @date Jun 17, 2016
 * @author Jordi G. March <jordi.garcia@colex.grupo-sm.com>
 */
public class MainServer {
	
	/** Log4Java */
	private static final Logger log = Logger.getLogger(MainServer.class);
	
	/** Basic connection attributes */
	private static String BASE_URI;
	private static String protocol;
	private static String host;
	private static String path;
	private static String port;
	
	/** HandlerList */
	private HandlerList handlers;

	/** Package with resources and providers for the REST API */
	private static final String REST_PACKAGE = "com.gruposm.backend.rest";

	/** Static setter for main properties */
	static {
		protocol = Config.getBackendProtocol();
		host = Config.getBackendHost();
		port = Config.getBackendPort();
		path = Config.getBackendWebroot();
		if (!path.endsWith("/")) {
			path += "/";
		}
		BASE_URI = protocol + host + ":" + port + path;
	}
	
	public MainServer() {
		handlers = new HandlerList();
	}

	/**
	 * Starts Jetty HTTP server exposing JAX-RS resources defined in this
	 * application.
	 */
	private void startServer() throws Exception {
		WebAppContext context = new WebAppContext();
		Server server = new Server();

        HttpConfiguration http_config = new HttpConfiguration();
        http_config.addCustomizer(new ForwardedRequestCustomizer());
        http_config.setSendServerVersion(false);

        ServerConnector http = new ServerConnector(server, new HttpConnectionFactory(http_config));
        http.setPort(Integer.parseInt(port));
        http.setIdleTimeout(30000);
        server.setConnectors(new Connector[]{http});
        
        context.setServer(server);
        context.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "" + Config.isDirListingAllowed());
        context.setErrorHandler(new ErrorHandler());
        server.setHandler(context);
        // default error handler for resources out of "context" scope
        server.addBean(new ErrorHandler());
        
        // Static resources...
        String mainPath = Config.getWebPrefixPath() + Config.getWebMainPath();
        String adminPath = Config.getWebPrefixPath() + Config.getWebAdminPath();
        // @see project's gulpfile.js...
        String scripts = "scripts/";
        String styles = "styles/";
        String images = "images/";
		String adminImg = "adminImg/";
        String fonts = "fonts/";
        // Remove default boardPath & map exclusively static resources but not index.html and the like...
        // addStaticHandler(Config.getWebMainPath(), boardPath);
        addStaticHandler(Config.getWebAdminPath(), adminPath);
        addStaticHandler(scripts, mainPath + scripts);
        addStaticHandler(styles, mainPath + styles);
        addStaticHandler(fonts, mainPath + fonts);
		addStaticHandler(images, mainPath + images);
		addStaticHandler(adminImg, adminPath + adminImg);

        // ServletContext -> Atmosphere + generic REST
        ServletContextHandler servletContextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        addJerseyCapabilities(servletContextHandler);
        
        // Add CORSFilter... 
        addCORSFilter(servletContextHandler);
        
        // Add Gzip Handler
        addGzipHandler(servletContextHandler);
        
        // Register Template
        TemplateEngineUtil.storeTemplateEngine(servletContextHandler.getServletContext());
        
        // At this stage, handlers should be built up correctly!
        // @see addStaticHandler + addGzipHandler
        server.setHandler(handlers);
        server.setStopAtShutdown(true);
        server.start();
	}

	/**
	 * Add a static context handler, based on webPath + real filePath, and adopting the following
	 * conventions:
	 * <ul>
	 * <li>webPath will be prepended with "/" + path + "/"</li>
	 * <li>filePath will be prepended with Config.getStaticAppPath</li>
	 * <li>No dir Allowed by default - Can be overriden using the fully qualified method</li>
	 * </ul>
	 *
	 * @param webPath ...
	 * @param filePath ...
	 */
	private void addStaticHandler(String webPath, String filePath) {
		addStaticHandler(webPath, filePath, Config.isDirListingAllowed());
	}

	/**
	 * Add a static context handler, based on webPath + real filePath, and adopting the following
	 * conventions:
	 * <ul>
	 * <li>webPath will be prepended with "/" + path + "/"</li>
	 * <li>filePath will be prepended with Config.getStaticAppPath</li>
	 * <li>dirAllowed to set specific directory listing allowed</li>
	 * </ul>
	 * 
	 * @param webPath ...
	 * @param filePath ...
	 * @param dirAllowed ...
	 */
	private void addStaticHandler(String webPath, String filePath, boolean dirAllowed) {
		ResourceHandler rh = new ResourceHandler();
        rh.setDirAllowed(dirAllowed);
        ContextHandler staticContextHandler = new ContextHandler();
        staticContextHandler.setContextPath(path + webPath);
        File dir = new File(Config.getStaticAppPath() + filePath);
        staticContextHandler.setBaseResource(Resource.newResource(dir));
        staticContextHandler.setHandler(rh);
        log.info("<addStaticHandler> Added " + Config.getBackendWebroot() + webPath + " --> " + filePath);

        // Add to list of handlers...
        handlers.addHandler(staticContextHandler);
	}

	
	private void addJerseyCapabilities(ServletContextHandler servletContextHandler) {
        ServletHolder jerseyServletHolder = new ServletHolder(new ServletContainer());
        jerseyServletHolder.setInitParameter(ServerProperties.PROVIDER_PACKAGES, REST_PACKAGE);
        // Need inject MultiPart for FileController...
        jerseyServletHolder.setInitParameter(ServerProperties.PROVIDER_CLASSNAMES, MultiPartFeature.class.getName());
        jerseyServletHolder.setInitOrder(1);
        
        servletContextHandler.addServlet(jerseyServletHolder, path + "*");
	}
	
	private void addCORSFilter(ServletContextHandler servletContextHandler) {
		FilterHolder holder = new FilterHolder(CrossOriginFilter.class);
		holder.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
		holder.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
		holder.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,POST,HEAD");
		holder.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin");
		holder.setName("cross-origin");
		FilterMapping fm = new FilterMapping();
		fm.setFilterName("cross-origin");
		fm.setPathSpec("*");
		servletContextHandler.addFilter(holder, "/*", EnumSet.of(DispatcherType.REQUEST));
	}
	
	private void addGzipHandler(ServletContextHandler servletContextHandler) {
		GzipHandler gzipHandler = new GzipHandler();
		gzipHandler.setIncludedMimeTypes("text/html", "text/plain", "text/xml", 
		    "text/css", "application/javascript", "text/javascript", "application/json");
		gzipHandler.setIncludedMethods("GET","POST");
		gzipHandler.setMinGzipSize(245);
		gzipHandler.setHandler(servletContextHandler);
		
		// Add this proper gzipHandler to handlerList
		handlers.addHandler(gzipHandler);
	}
	
	/**
	 * Method for registering servlets.... Quite easy...
	 * 
	 * @param context ...
	 * @param type ...
	 * @throws InstantiationException ..
	 * @throws IllegalAccessException ...
	 * @throws InvocationTargetException ...
	 * @throws NoSuchMethodException ...
	 */
	private void addServlet(ServletContextHandler context, Class<? extends HttpServlet> type) 
			throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		WebServlet info = type.getAnnotation(WebServlet.class);
		for (String servletPath : info.value()) {
			HttpServlet servlet = type.getConstructor().newInstance();
			if (servletPath == null || servletPath.equals("") || servletPath.equals("/")) {
				context.addServlet(new ServletHolder(servlet), servletPath);
			} else {
				context.addServlet(new ServletHolder(servlet), path + servletPath);
			}
		}
	}
	
	/**
	 * Builds database on demand. The only prerequisit is a void database instance with
	 * full access - Details at ConnectionFactory / ldannot.properties
	 * 
	 * @see ConnectionFactory
	 */
	private void buildDatabase() {
		DBCreatorService dbs = new DBCreatorService();
		dbs.createDataBase();
		
		// Alters en caso de cambios con datos en producción...
		dbs.performAlters();
	}
	
	/**
     * Dummy error handler that disables any error pages or jetty related messages and returns our
     * custom Error Page, coupled with the error message and the like....
     */
    private static class ErrorHandler extends ErrorPageErrorHandler {
    	
    	private void prepareResponse(HttpServletRequest request, HttpServletResponse response) {
    		response.setContentType("text/html;charset=UTF-8");
      		response.setHeader("Pragma", "no-cache");
      		response.setHeader("Cache-Control", "no-cache");
      		response.setDateHeader("Expires", 0);
            log.debug("<prepareResponse> Error code = " + response.getStatus());
    	}
    	
        @Override
        public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) {
        	prepareResponse(request, response);
        	String errorMessage = response.toString();
			if (response.getStatus() >= 500) {
				errorMessage = baseRequest.getAttribute(RequestDispatcher.ERROR_MESSAGE).toString();
			}
        	// Msg & Exception
			String errorException = errorMessage;
        	errorMessage = errorMessage.substring(errorMessage.lastIndexOf("xception:") + 10);

            String errorPage = "error500";
            if (response.getStatus() <= 404) {
            	if (response.getStatus() == 403) {
            		errorPage = "error403";
            	} else if (response.getStatus() == 401) {
            		errorPage = "error401";
            	} else {
            		errorPage = "error404";
            	}
            }
            // Redirect based on exception inspection:
            if (errorException.contains(AuthException.class.getName())) {
            	errorPage = "error403";
            } else if (errorException.contains(UnauthorizedException.class.getName())) {
            	errorPage = "error401";
            }

			// Redirect to a single page showing errors...
            // Use Thymeleaf for templating purposes...
            try {
            	TemplateEngine engine = TemplateEngineUtil.getTemplateEngine(request.getServletContext(), TemplateEngineUtil.TEMPLATE_ADMIN);
            	WebContext ctx = new WebContext(request, response, request.getServletContext(), request.getLocale());
            	ctx.setVariable("errorMessage", errorMessage);

            	// i18n on demand
            	String language = request.getLocale().getLanguage();
            	String lang = request.getParameter(Constants.LOCALE);
            	if (lang != null) language = lang; // override via parameter...
            	LocaleUtils.getInstance().adaptDefaultMessages(engine, language);
            	
            	engine.process(errorPage, ctx, response.getWriter());
            } catch (Exception ignore) { } 
        }
    }
    
    
	/**
	 * Main method.
	 * 
	 * @param args ...
	 */
	public static void main(String[] args) {
		try {
			MainServer server = new MainServer();
			server.startServer();
			String text = "SM Generic Backend Server started with WADL available at " + BASE_URI
					+ "application.wadl\n";
			log.info("<main>:" + text);
			
			// Build database tables...
			server.buildDatabase();

		} catch (Exception e) {
			System.out.println("Error starting SM Generic Backend Server!");
			e.printStackTrace();
		}
	}
}
